# README #

This is a personal app that I want to continue developing. A social network app named NanoPic.
Still working on it.

Some screenshots from app runtime:

Login page: 
It uses a generated id and a password right now (I'll replace the id with a username or e-mail).
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/login.JPG)


Sign up page: create a new account:
It generates the UID for login after creating the account. The childhood nickname is for password recovery.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/signup.JPG)


Profile page:
After logging in, this is the first page. Right now it's simple, but I will update it in the future.
There will be an upload profile picture and some more info about the user.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/profilePage.JPG)


Find friends page:
Search for users, send them friend requests or accept their friend requests.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/findFriends.JPG)


Friend requests Page:
Shows all received and sent friend requests. Allows to accept or reject the received ones, and cancel the sent ones. 
I'll update this page to show the user's profile when looking through the requests.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/friendRequests.JPG)


Chat Page:
Chat with a friend by clicking on the profiles in your friends list. The reply functionality is available now with one click on the message.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/chat.JPG)


Group chats can be created as well:
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/groupChats.JPG)


Planning an event? Create it on the platform and post it so other users can see it and participate.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/createEvent.JPG)


You can subscribe to an event. 
Turn the notifications on for reminders about the event when its date is coming closer. You can turn off the notifications as well.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/events.JPG)


Do you want to see your activity in the past 2 weeks? Or more? With a click you can download a pdf document with your activity log in a period of time.
You can add your messages, your new friendships, or both.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/activityLog.JPG)


The notifications icon is available. If you have any new notifications it will be yellow and their number will be displayed.
Click it and you'll see all your new notifications. Click the one you're interested and it will bring you to the associated page.
![Alt text](https://bitbucket.org/chstr_mihai/socialnetwork/raw/16fff29062f3572e387e630420fcef4387fe494a/src/main/resources/screenshotsFromApp/notifications.JPG)
