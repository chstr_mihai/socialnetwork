package socialnetwork.utils.events;

public class MessageEvent implements Event {
    private EventType type;

    public MessageEvent(EventType type) {
        this.type = type;
    }

    @Override
    public EventType getType() {
        return type;
    }

}
