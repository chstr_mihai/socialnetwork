package socialnetwork.utils.events;

public class NotificationEvent implements Event {
    private EventType type;

    public NotificationEvent(EventType type) {
        this.type = type;
    }

    @Override
    public EventType getType() {
        return type;
    }
}
