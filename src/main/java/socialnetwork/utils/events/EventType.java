package socialnetwork.utils.events;

public enum EventType {
    FRIENDREQUEST, MESSAGE, FRIENDSHIP, SCHEDULEDEVENT, NOTIFICATION;
}
