package socialnetwork.utils.events;

public class FriendRequestEvent implements Event {
    private EventType type;

    public FriendRequestEvent(EventType type) {
        this.type = type;
    }

    @Override
    public EventType getType() {
        return type;
    }

}
