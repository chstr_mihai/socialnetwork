package socialnetwork.utils.events;

public class FriendshipEvent implements Event {
    private EventType type;

    public FriendshipEvent(EventType type) {
        this.type = type;
    }

    @Override
    public EventType getType() {
        return type;
    }

}
