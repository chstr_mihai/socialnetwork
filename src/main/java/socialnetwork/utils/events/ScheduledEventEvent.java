package socialnetwork.utils.events;

public class ScheduledEventEvent implements Event {
    private EventType type;

    public ScheduledEventEvent(EventType type) {
        this.type = type;
    }

    @Override
    public EventType getType() {
        return type;
    }
}
