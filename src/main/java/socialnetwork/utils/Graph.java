package socialnetwork.utils;

import socialnetwork.domain.Tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
    int V, components_count, longest_component_index, longest_component_length, len;
    List<List<Integer>> adjListArray;
    List<Tuple<Long, Long>> friendshipList;
    Map<Long, Integer> nodes;
    Map<Integer, Long> inverted_nodes;
    List<Long> longest_component;

    public Graph(int V, Map<Long, Integer> usersIds, List<Tuple<Long, Long>> friendships) {
        this.V = V;
        this.components_count = 0;
        this.longest_component_length = 0;
        this.longest_component_index = -1;
        this.friendshipList = friendships;
        this.nodes = usersIds;
        this.longest_component = new ArrayList<>();
        adjListArray = new ArrayList<>();

        this.inverted_nodes = new HashMap<>();
        nodes.forEach((x, y) -> inverted_nodes.put(y, x));

        for (int i = 0; i < V; i++)
            adjListArray.add(new ArrayList<Integer>());


        friendshipList.forEach(x -> addEdge(nodes.get(x.getLeft()), nodes.get(x.getRight())));

    }

    private void addEdge(int src, int dest) {

        adjListArray.get(src).add(dest);
        adjListArray.get(dest).add(src);

    }

    private void DFSUtil(int v, boolean[] visited) {

        visited[v] = true;
        for (int x : adjListArray.get(v)) {
            if (!visited[x]) {
                len++;
                DFSUtil(x, visited);
            }
        }

    }

    private void connectedComponents() {
        // Mark all the vertices as not visited
        boolean[] visited = new boolean[V];
        for (int v = 0; v < V; ++v) {
            if (!visited[v]) {
                // print all reachable vertices
                // from v
                len = 1;
                DFSUtil(v, visited);
                if (len > longest_component_length) {
                    longest_component_length = len;
                    longest_component_index = v;
                }
                components_count++;
            }
        }
    }

    public int getConnectedComponentsCount() {

        connectedComponents();
        return components_count;

    }

    public void finalDFS(int v, boolean[] visited) {

        visited[v] = true;
        longest_component.add(inverted_nodes.get(v));
        for (int x : adjListArray.get(v)) {
            if (!visited[x]) {
                finalDFS(x, visited);
            }
        }

    }

    public List<Long> getLongestComponent() {

        connectedComponents();
        boolean[] visited = new boolean[V];
        finalDFS(longest_component_index, visited);
        return longest_component;

    }

}