package socialnetwork;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.LoginController;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.*;
import socialnetwork.ui.Console;

import java.io.IOException;

public class MainFX extends Application {
    String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url")
            , username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username")
            , password = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");

    Repository<Long, User> userDbRepository = new UserDbRepository(url, username, password);
    Repository<Tuple<Long, Long>, Friendship> friendshipDbRepository = new FriendshipDbRepository(url, username, password);
    Repository<Tuple<Long, Long>, FriendRequest> friendRequestDbRepository = new FriendRequestDbRepository(url, username
            , password);
    Repository<Long, Message> messageDbRepository = new MessageDbRepository(url, username, password);
    Repository<Long, Notification> notificationDbRepository = new NotificationDbRepository(url, username, password);
    PagingRepository<Long, ScheduledEvent> eventRepository = new EventDbRepository(url, username, password);
    Repository<Tuple<Long, Long>, Participant> participantRepository = new ParticipantsDbRepository(url, username, password);

    UserValidator userValidator = new UserValidator();

    FriendshipValidator friendshipValidator = new FriendshipValidator();

    UserDbService userDbService = new UserDbService(userDbRepository, userValidator, friendshipDbRepository);
    FriendshipDbService friendshipDbService = new FriendshipDbService(friendshipDbRepository, friendshipValidator
            , userDbRepository);

    FriendRequestValidator friendRequestValidator = new FriendRequestValidator();
    FriendRequestDbService friendRequestDbService = new FriendRequestDbService(friendshipDbRepository, friendRequestValidator
            , userDbRepository, friendRequestDbRepository);

    Validator<Message> messageValidator = new MessageValidator();
    MessageDbService messageDbService = new MessageDbService(messageDbRepository, messageValidator, userDbRepository
            , friendshipDbRepository);

    NotificationDbService notificationDbService = new NotificationDbService(notificationDbRepository);

    EventDbService eventService = new EventDbService(eventRepository, participantRepository);

    //Console console = new Console(userDbService, friendshipDbService, friendRequestDbService, messageDbService);


    @Override
    public void start(Stage s) throws Exception {

        initStage();
        initStage();

    }

    void initStage() throws IOException {

        Stage stage = new Stage();
        stage.getIcons().add(new Image("/images/NanoPicNoBck.png"));

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/login.fxml"));
        AnchorPane root=loader.load();

        LoginController loginController = loader.getController();
        loginController.setService(userDbService, friendshipDbService, friendRequestDbService, messageDbService, notificationDbService, eventService, stage);

        stage.setScene(new Scene(root));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("Login");
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}