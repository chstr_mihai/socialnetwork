package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;
import socialnetwork.exceptions.ValidationException;

public class FriendRequestValidator implements Validator<FriendRequest> {
    @Override
    public void validate(FriendRequest entity) throws ValidationException {
        String exp = "";
        if (entity.getId().getLeft() <= 0 || entity.getId().getRight() <= 0)
            exp += "The ids cannot be negative or null\n";
        if (entity.getId().getRight().equals(entity.getId().getLeft()))
            exp += "The friend request can't be send to this user\n";
        if (exp.length() > 0)
            throw new ValidationException(exp);
    }
}
