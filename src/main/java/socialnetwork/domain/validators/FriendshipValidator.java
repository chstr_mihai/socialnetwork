package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;
import socialnetwork.exceptions.ValidationException;

public class FriendshipValidator implements Validator<Friendship> {
    @Override
    public void validate(Friendship entity) throws ValidationException {
        String exp = "";
        if (entity.getId().getLeft() <= 0 || entity.getId().getRight() <= 0)
            exp += "The ids cannot be negative or null\n";
        if (entity.getId().getRight().equals(entity.getId().getLeft()))
            exp += "The ids must be different!\n";
        if (exp.length() > 0)
            throw new ValidationException(exp);
    }
}
