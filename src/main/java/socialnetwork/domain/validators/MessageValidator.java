package socialnetwork.domain.validators;

import socialnetwork.domain.Message;
import socialnetwork.exceptions.ValidationException;

public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        String exp = "";
        if(entity.getTo_user().contains(entity.getFrom_user()))
            exp += "You can't send the message to you\n";
        if(entity.getText().equals(""))
            exp += "The message mustn't be empty\n";
        if(entity.getRepliedMessage() != null)
            if(!entity.getRepliedMessage().getTo_user().contains(entity.getFrom_user()))
                exp += "You can't reply to this message\n";

        if(exp.length() > 0)
            throw new ValidationException(exp);
    }
}
