package socialnetwork.domain.validators;

import socialnetwork.domain.User;
import socialnetwork.exceptions.ValidationException;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String exp = "";
        if (entity.getFirstName().equals("") || !Character.isUpperCase(entity.getFirstName().charAt(0)))
            exp += "Invalid user's first name!\n";
        if (entity.getLastName().equals("") || !Character.isUpperCase(entity.getLastName().charAt(0)))
            exp += "Invalid user's last name!\n";
        if(entity.getPassword().equals(""))
            exp += "Invalid password\n";
        if (exp.length() > 0)
            throw new ValidationException(exp);
    }
}
