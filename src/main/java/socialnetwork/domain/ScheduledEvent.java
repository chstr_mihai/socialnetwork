package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;

public class ScheduledEvent extends Entity<Long> {

    private String name;
    private String description;
    private String location;
    private LocalDateTime datetime;
    private List<User> joinedUsers;

    public ScheduledEvent(Long eid, String name, String description, String location, LocalDateTime datetime, List<User> joinedUsers) {

        this.name = name;
        this.datetime = datetime;
        this.joinedUsers = joinedUsers;
        this.description = description;
        this.location = location;
        setId(eid);

    }

    public ScheduledEvent(String name, String description, String location, LocalDateTime datetime, List<User> joinedUsers) {

        this.name = name;
        this.description = description;
        this.location = location;
        this.datetime = datetime;
        this.joinedUsers = joinedUsers;

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public List<User> getJoinedUsers() {
        return joinedUsers;
    }

    public void join(User newUser) {
        joinedUsers.add(newUser);
    }

    public void leave(User user) {
        joinedUsers.remove(user);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ScheduledEvent)) return false;
        ScheduledEvent that = (ScheduledEvent) obj;
        return getId().equals(that.getId());
    }
}
