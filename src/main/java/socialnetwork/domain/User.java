package socialnetwork.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User extends Entity<Long> implements Comparable<User> {
    private String firstName;
    private String lastName;
    private String password;
    private String childhoodNickname;

    private final List<User> friends = new ArrayList<User>() {
        @Override
        public String toString() {
            StringBuilder printer = new StringBuilder();
            for (User u : friends)
                printer.append(" ").append(u.getId());
            return printer.toString();
        }
    };

    public User(String firstName, String lastName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public User(String firstName, String lastName, Long id, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        setId(id);
    }

    public User(String firstName, String lastName, Long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        setId(id);
    }

    public User(String firstName, String lastName, Long id, String password, String childhoodNickname) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.childhoodNickname = childhoodNickname;
        setId(id);
    }

    public User(String firstName, String lastName, String password, String childhoodNickname) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.childhoodNickname = childhoodNickname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<User> getFriends() {
        return friends;
    }

    public String getChildhoodNickname() {
        return childhoodNickname;
    }

    public void setChildhoodNickname(String childhoodNickname) {
        this.childhoodNickname = childhoodNickname;
    }

    public void addFriend(User user) {

        friends.add(user);

    }

    public void deleteFriend(User user) {

        friends.remove(user);

    }

    @Override
    public String toString() {

        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User that = (User) o;
        return getId().equals(that.getId()) &&
                getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }

    @Override
    public int compareTo(User o) {
        return this.getId().compareTo(((User) o).getId());
    }
}