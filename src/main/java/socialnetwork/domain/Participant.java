package socialnetwork.domain;

public class Participant extends Entity<Tuple<Long, Long>> {
    private boolean notificationsOn;

    public Participant(Long eid, Long uid, boolean notificationsOn) {
        this.notificationsOn = notificationsOn;
        setId(new Tuple<>(eid, uid));
    }

    public Participant(Tuple<Long, Long> pid) {
        this.notificationsOn = true;
        setId(pid);
    }

    public boolean isNotificationsOn() {
        return notificationsOn;
    }

    public void setNotificationsOn(boolean notificationsOn) {
        this.notificationsOn = notificationsOn;
    }

}
