package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;

public class ReplyMessage extends Message {
    private final Message repliedMessage;

    public ReplyMessage(User from_user, List<User> to_user, String text, Message repliedMessage, boolean isGroupMesage) {
        super(from_user, to_user, text, isGroupMesage);

        this.repliedMessage = repliedMessage;
    }

    public ReplyMessage(Long id, User from_user, List<User> to_user, String text, LocalDateTime date
            , Message repliedMessage, boolean isGroupMesage) {
        super(id, from_user, to_user, text, date, isGroupMesage);

        this.repliedMessage = repliedMessage;
    }

    public ReplyMessage(Message message, Message repliedMessage) {
        super(message.getId(), message.getFrom_user(), message.getTo_user(), message.getText(), message.getDate()
                , message.isGroupMessage());

        this.repliedMessage = repliedMessage;
    }

    @Override
    public Message getRepliedMessage() {
        return repliedMessage;
    }
}
