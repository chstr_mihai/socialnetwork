package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Message extends Entity<Long> {
    User from_user;
    List<User> to_user;
    String text;
    LocalDateTime date;
    boolean isGroupMessage;

    public Message(User from_user, List<User> to_user, String text, boolean isGroupMesage) {
        this.from_user = from_user;
        this.to_user = to_user;
        this.text = text;
        this.date = LocalDateTime.now();
        this.isGroupMessage = isGroupMesage;
    }

    public Message(Long  id, User from_user, List<User> to_user, String text, LocalDateTime date, boolean isGroupMesage) {
        this.from_user = from_user;
        this.to_user = to_user;
        this.text = text;
        this.date = date;
        this.isGroupMessage = isGroupMesage;
        setId(id);
    }

    public User getFrom_user() {
        return from_user;
    }

    public List<User> getTo_user() {
        return to_user;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public boolean isGroupMessage() {
        return isGroupMessage;
    }

    public void setGroupMessage(boolean groupMessage) {
        isGroupMessage = groupMessage;
    }

    @Override
    public String toString() {
        return from_user.getFirstName() + " " + from_user.getLastName() + ": " + text +
                "\n     sent at: " + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    public Message getRepliedMessage() {
        return null;
    }

}
