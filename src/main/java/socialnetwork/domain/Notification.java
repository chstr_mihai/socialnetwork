package socialnetwork.domain;

import java.time.LocalDateTime;

public class Notification extends Entity<Long> {

    private Long targetedUser;
    private String text;
    private String type;
    private boolean isOpened;
    private LocalDateTime date;

    public Notification(Long nid, Long targetedUser, String text, String type, boolean isOpened, LocalDateTime date) {
        this.targetedUser = targetedUser;
        this.text = text;
        this.type = type;
        this.isOpened = isOpened;
        this.date = date;

        setId(nid);
    }

    public Notification(Long targetedUser, String text, String type, boolean isOpened, LocalDateTime date) {
        this.targetedUser = targetedUser;
        this.text = text;
        this.type = type;
        this.isOpened = isOpened;
        this.date = date;
    }

    public Long getTargetedUser() {
        return targetedUser;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return text;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Notification)) return false;
        Notification that = (Notification) obj;
        return getText().equals(that.getText());
    }
}
