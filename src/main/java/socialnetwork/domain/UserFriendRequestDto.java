package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserFriendRequestDto extends Entity<Tuple<Long, Long>> {
    private String firstName;
    private String lastName;
    private String status;
    private LocalDateTime date;
    private String dateString;
    private String fullName;
    private Long senderId;
    private Long receiverId;

    public UserFriendRequestDto(Long u1id, Long u2id, String firstName, String lastName, String status, LocalDateTime dateString) {
        setId(new Tuple<>(u1id, u2id));
        this.senderId = u1id;
        this.receiverId = u2id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.date = dateString;
        this.dateString = dateString.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        fullName = firstName + " " + lastName;
    }

    public UserFriendRequestDto(String firstName, String lastName, String status, String dateString, String fullName, Long senderId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.dateString = dateString;
        this.fullName = fullName;
        this.senderId = senderId;
    }

    public String getFullName() {return fullName;}

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDateString() {
        return dateString;
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    @Override
    public String toString() {
        return "UserFriendRequestDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", status='" + status + '\'' +
                ", dateString='" + dateString + '\'' +
                ", fullName='" + fullName + '\'' +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof UserFriendRequestDto)) return false;
        UserFriendRequestDto that = (UserFriendRequestDto) obj;
        return getId().getLeft().equals(that.getId().getLeft()) &&
                getId().getRight().equals(that.getId().getRight());
    }
}
