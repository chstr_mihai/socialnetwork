package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserDateDto {
    Long id;
    private String firstName;
    private String lastName;
    private LocalDateTime date;
    private String dateString;

    public UserDateDto(Long id, String firstName, String lastName, LocalDateTime date) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.dateString = this.date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDateString() {
        return dateString;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public String getStringForLog() {
        return "You and " + firstName + " " + lastName + " became friends on " + dateString;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof UserDateDto)) return false;
        UserDateDto that = (UserDateDto) obj;
        return getId().equals(that.getId());
    }
}
