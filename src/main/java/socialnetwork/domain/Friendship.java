package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class Friendship extends Entity<Tuple<Long, Long>> {
    LocalDateTime date;

    public Friendship(Long id1, Long id2) {
        setId(new Tuple<>(id1, id2));
        date = LocalDateTime.now();
    }

    public Friendship(Long id1, Long id2, LocalDateTime date) {
        setId(new Tuple<>(id1, id2));
        this.date = date;
    }

    @Override
    public String toString() {
        return "FID: " + getId() + ", became friends on: " + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    /**
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Friendship)) return false;
        Friendship that = (Friendship) o;
        return this.getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate());
    }
}
