package socialnetwork.domain;

import jdk.vm.ci.meta.Local;

import java.time.LocalDateTime;

public class FriendRequest extends Entity<Tuple<Long, Long>> {
    private String status;
    private LocalDateTime date;

    public FriendRequest(Long from, Long to) {
        this.status = "pending";
        setId(new Tuple<>(from, to));
        this.date = LocalDateTime.now();
    }

    public FriendRequest(Long from, Long to, String status, LocalDateTime date) {
        this.status = status;
        setId(new Tuple<>(from, to));
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Friend request from: " + getId().getLeft() +
                " to: " + getId().getRight() +
                ", status: " + status;
    }
}

