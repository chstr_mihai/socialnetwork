package socialnetwork.controller;

import socialnetwork.domain.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Page {

    private String firstName;
    private String lastName;
    private List<UserDateDto> friendsList;
    private List<List<User>> groupChats;
    private List<User> nonFriends;
    private List<UserFriendRequestDto> receivedFriendRequests;
    private List<UserFriendRequestDto> sentFriendRequests;
    private List<Notification> notifications;
    private List<ScheduledEvent> events;

    public Page(String firstName, String lastName, List<UserDateDto> friendsList, Iterable<List<User>> groupChats
            , Iterable<User> nonFriends, Iterable<UserFriendRequestDto> receivedFriendRequests
            , Iterable<UserFriendRequestDto> sentFriendRequests, Iterable<Notification> notifications
            , List<ScheduledEvent> events) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.friendsList = friendsList;
        this.groupChats = StreamSupport.stream(groupChats.spliterator(), false).collect(Collectors.toList());
        this.nonFriends = StreamSupport.stream(nonFriends.spliterator(), false).collect(Collectors.toList());
        this.receivedFriendRequests = StreamSupport.stream(receivedFriendRequests.spliterator(), false).collect(Collectors.toList());
        this.sentFriendRequests = StreamSupport.stream(sentFriendRequests.spliterator(), false).collect(Collectors.toList());
        this.notifications = StreamSupport.stream(notifications.spliterator(), false).collect(Collectors.toList());
        this.events = events;

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<UserDateDto> getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(List<UserDateDto> friendsList) {
        this.friendsList = friendsList;
    }

    public List<List<User>> getGroupChats() {
        return groupChats;
    }

    public void setGroupChats(List<List<User>> groupChats) {
        this.groupChats = groupChats;
    }

    public List<User> getNonFriends() {
        return nonFriends;
    }

    public void setNonFriends(List<User> nonFriends) {
        this.nonFriends = nonFriends;
    }

    public List<UserFriendRequestDto> getReceivedFriendRequests() {
        return receivedFriendRequests;
    }

    public void setReceivedFriendRequests(List<UserFriendRequestDto> receivedFriendRequests) {
        this.receivedFriendRequests = receivedFriendRequests;
    }

    public List<UserFriendRequestDto> getSentFriendRequests() {
        return sentFriendRequests;
    }

    public void setSentFriendRequests(List<UserFriendRequestDto> sentFriendRequests) {
        this.sentFriendRequests = sentFriendRequests;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public List<ScheduledEvent> getEvents() {
        return events;
    }

    public void setEvents(List<ScheduledEvent> events) {
        this.events = events;
    }
}
