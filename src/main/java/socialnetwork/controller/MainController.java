package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.*;
import socialnetwork.exceptions.RepoException;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.exceptions.ValidationException;
import socialnetwork.service.*;
import socialnetwork.statistics.UserActivityLog;
import socialnetwork.utils.events.*;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.time.temporal.ChronoUnit.DAYS;

public class MainController implements Observer<Event> {

    public AnchorPane paneReceivedFriendRequests;
    public AnchorPane paneSentFriendRequests;
    public AnchorPane anchorPaneFriendRequests;
    public AnchorPane anchorPaneProfile;
    public AnchorPane anchorPaneFindFriends;
    public AnchorPane paneOtherUserProfile;
    public AnchorPane anchorPaneChat;
    public AnchorPane anchorPaneChatProfile;
    public AnchorPane paneNanoPic;
    public AnchorPane anchorPaneNewGroup;
    public AnchorPane anchorPaneGroupChat;
    public AnchorPane anchorPaneActivityLog;
    public AnchorPane paneDonePDF;

    public ScrollPane scrollPaneMessages;

    public TableView<UserFriendRequestDto> tableViewReceivedRequests;
    public TableColumn<UserFriendRequestDto, String> columnFromReceivedRequest;
    public TableColumn<UserFriendRequestDto, String> columnStatusReceivedRequest;
    public TableColumn<UserFriendRequestDto, String> columnDateReceivedRequest;

    public TableView<UserFriendRequestDto> tableViewSentFriendRequests;
    public TableColumn<UserFriendRequestDto, String> columnSentToFriendRequest;
    public TableColumn<UserFriendRequestDto, String> columnStatusSentFriendRequest;
    public TableColumn<UserFriendRequestDto, String> columnDateSentFriendRequest;

    public ListView<UserDateDto> listViewFriends;
    public ListView<User> listViewFindFriends;
    public ListView<List<User>> listViewGroupChats;
    public ListView<UserDateDto> listViewNewGroupMembers;
    public ListView<String> listViewActivityLog;
    public ListView<Notification> listViewNotifications;

    public Label labelReceivedFriendRequestError;
    public Label labelSentFriendRequestError;
    public Label labelOtherUserProfileUsername;
    public Label labelProfileUsername;
    public Label labelNanoPic;
    public Label labelChatProfileUsername;
    public Label labelFindFriendsRequestStatus;
    public Label labelChatUsername;
    public Label labelReplyTo;
    public Label labelGenerateActivityLogError;
    public Label labelDonePDFMessage;
    public Label labelNotificationsCount;

    public Button buttonSendMessage;
    public Button buttonSendFriendRequest;
    public Button buttonFindFriendAcceptRequest;
    public Button buttonFindFriendRejectRequest;
    public Button buttonChat;
    public Button buttonFriendRequests;
    public Button buttonFindFriends;
    public Button buttonProfile;
    public Button buttonNotifications;

    public TextField textFieldFindFriends;
    public TextField textFieldTypeMessage;
    public TextField textFieldSearchInFriends;
    public TextField textFieldFileNameActivityLog;

    public VBox vBoxChat;

    public ImageView imageFindFriendsProfile;

    public RadioButton radioButtonFriendships;
    public RadioButton radioButtonMessages;
    public RadioButton radioButtonFriendMessages;

    public DatePicker datePickerActivityLogFrom;
    public DatePicker datePickerActivityLogTo;
    public AnchorPane anchorPaneEvents;
    public ListView<ScheduledEvent> listViewEvents;
    public TextField textFieldSearchEvents;
    public AnchorPane paneEventInfo;
    public AnchorPane paneCreateEvent;
    public TextField textFieldEventName;
    public TextField textFieldEventLocation;
    public TextArea textAreaEventDescription;
    public DatePicker datePickerEventDate;
    public TextField textFiledEventHour;
    public TextField textFieldEventMinute;
    public Label labelEventCreateError;
    public Label labelEventInfoName;
    public TextArea textAreaEventInfoDescription;
    public Label labelEventInfoLocation;
    public Label labelEventInfoDate;
    public ListView<User> listViewEventInfoUsersJoined;
    public Button buttonJoinEvent;
    public Button buttonNotificationsSettingsEvent;
    public Button buttonEvents;

    ObservableList<UserDateDto> modelFriends = FXCollections.observableArrayList();
    ObservableList<User> modelNonFriends = FXCollections.observableArrayList();
    ObservableList<UserFriendRequestDto> modelReceivedFriendRequests = FXCollections.observableArrayList();
    ObservableList<UserFriendRequestDto> modelSentFriendRequests = FXCollections.observableArrayList();
    ObservableList<List<User>> modelGroupChats = FXCollections.observableArrayList();
    ObservableList<UserDateDto> modelNewGroupMembers = FXCollections.observableArrayList();
    ObservableList<String> modelActivityLog = FXCollections.observableArrayList();
    ObservableList<Notification> modelNotifications = FXCollections.observableArrayList();
    ObservableList<ScheduledEvent> modelEvents = FXCollections.observableArrayList();
    ObservableList<User> modelEventJoined = FXCollections.observableArrayList();

    List<UserDateDto> newGroupMembersList = new ArrayList<>();
    List<UserDateDto> activityLogFriendships;
    List<Message> activityLogMessages;
    List<Notification> notifications = new ArrayList<>();

    private Long selectedIdToReply = (long)0;

    private Stage mainStage;
    private User loggedInUser;
    private MouseEvent mousePressed;

    private String chatReceiver = "";
    private UserDateDto currentFriend = null;
    private List<User> currentGroup = null;

    private UserDbService userService;
    private FriendshipDbService friendshipService;
    private FriendRequestDbService friendRequestService;
    private MessageDbService messageService;
    private NotificationDbService notificationService;
    private EventDbService eventService;

    private Page userPage;

    public void setEnvironment(UserDbService userService, FriendshipDbService friendshipService
            , FriendRequestDbService friendRequestService, MessageDbService messageService
            , NotificationDbService notificationService, EventDbService eventService, Stage stage, User loggedInUser, Page userPage) {

        labelNanoPic.setText("C:\\NanoPic\\" + loggedInUser.getFirstName().toLowerCase()
                + loggedInUser.getLastName().toLowerCase() + ">");
        paneNanoPic.setPrefWidth(labelNanoPic.getWidth());
        labelProfileUsername.setText(loggedInUser.getFirstName() + " " + loggedInUser.getLastName());
        this.loggedInUser = loggedInUser;

        this.mainStage = stage;
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.friendRequestService = friendRequestService;
        this.messageService = messageService;
        this.notificationService = notificationService;
        this.eventService = eventService;
        this.userPage = userPage;

        friendRequestService.addObserver(this);
        messageService.addObserver(this);
        friendshipService.addObserver(this);
        eventService.addObserver(this);
        notificationService.addObserver(this);

        this.modelFriends.setAll(userPage.getFriendsList());
        this.modelNonFriends.setAll(userPage.getNonFriends());
        this.modelReceivedFriendRequests.setAll(userPage.getReceivedFriendRequests());
        this.modelSentFriendRequests.setAll(userPage.getSentFriendRequests());
        this.modelGroupChats.setAll(userPage.getGroupChats());
        this.modelNotifications.setAll(userPage.getNotifications());
        this.modelEvents.setAll(userPage.getEvents());

        scrollPaneMessages.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        buttonNotifications.setOnMousePressed(x -> buttonNotifications.setStyle("-fx-background-image: url('/images/not2.png')"));
        buttonNotifications.setOnMouseReleased(x -> buttonNotifications.setStyle("-fx-background-image: url('/images/not1.png')"));

        setEventsNotifications();
        setNotificationsButton();

        textAreaEventDescription.setWrapText(true);
        textAreaEventInfoDescription.setWrapText(true);

    }

    @FXML
    public void initialize() {

        listViewFriends.setItems(modelFriends);
        listViewFindFriends.setItems(modelNonFriends);
        listViewGroupChats.setItems(modelGroupChats);
        listViewNotifications.setItems(modelNotifications);
        listViewEvents.setItems(modelEvents);
        listViewEventInfoUsersJoined.setItems(modelEventJoined);

        columnFromReceivedRequest.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        columnStatusReceivedRequest.setCellValueFactory(new PropertyValueFactory<>("status"));
        columnDateReceivedRequest.setCellValueFactory(new PropertyValueFactory<>("dateString"));
        tableViewReceivedRequests.setItems(modelReceivedFriendRequests);

        columnSentToFriendRequest.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        columnStatusSentFriendRequest.setCellValueFactory(new PropertyValueFactory<>("status"));
        columnDateSentFriendRequest.setCellValueFactory(new PropertyValueFactory<>("dateString"));
        tableViewSentFriendRequests.setItems(modelSentFriendRequests);

        textFieldFindFriends.textProperty().addListener(x -> handleFilterFindFriend());
        textFieldSearchInFriends.textProperty().addListener(x -> handleFilterFriends());
        textFieldSearchEvents.textProperty().addListener(x -> handleFilterEvents());

    }

    private String setDaysToEvent(LocalDateTime now, LocalDateTime eventDate) {

        long days = DAYS.between(now, eventDate) + 1;
        if(days > 3)
            return "";
        if(days > 1)
            return " 3 days";
        if(days > 0)
            return " 1 day";

        return "";

    }

    private void setEventsNotifications() {

        List<ScheduledEvent> events = eventService.getEventsWithNotificationsOn(loggedInUser);

        List<Notification> newNotifications = new ArrayList<>();
        events.forEach(x -> {

            String days = setDaysToEvent(LocalDateTime.now(), x.getDatetime());
            if (!days.equals("")) {

                Notification notification = new Notification(loggedInUser.getId(), x.getName() + " event happening in less than"
                        + days, "Event", false, LocalDateTime.now());
                if(!modelNotifications.contains(notification))
                    newNotifications.add(notification);

            }
        });

        notificationService.addMultiple(newNotifications);

    }

    @Override
    public void update(Event event) {

        if(chatReceiver.equals("user"))
            setMessages(currentFriend);
        if(chatReceiver.equals("group"))
            setGroupMessages(currentGroup);

        if(event.getType() == EventType.FRIENDSHIP) {
            setFriends();
            setFindFriends();
        }

        else if(event.getType() == EventType.FRIENDREQUEST) {
            setFriends();
            setFindFriends();
            setReceivedFriendRequests();
            setSentFriendRequests();
        }

        else if(event.getType() == EventType.MESSAGE)
            setGroupChats();

        else if(event.getType() == EventType.SCHEDULEDEVENT)
            setEvents();

        setNotifications();
        setNotificationsButton();
        scrollPaneMessages.setVvalue(1.0);

        listViewFriends.getSelectionModel().select(listViewFriends.getSelectionModel().getSelectedItem());
        listViewFindFriends.getSelectionModel().select(listViewFindFriends.getSelectionModel().getSelectedItem());
        listViewEvents.getSelectionModel().select(listViewEvents.getSelectionModel().getSelectedItem());
        listViewGroupChats.getSelectionModel().select(listViewGroupChats.getSelectionModel().getSelectedItem());

    }

    private void setNotificationsButton() {

        notifications = notificationService.getUsersNotifications(loggedInUser.getId());
        int unopenedNotificationCount = (int) notifications
                .stream().filter(x -> !x.isOpened()).count();
        if(unopenedNotificationCount == 0) {
            labelNotificationsCount.setVisible(false);
            buttonNotifications.setStyle("-fx-background-image: url('/images/not1.png')");
        }
        else {
            labelNotificationsCount.setVisible(true);
            labelNotificationsCount.setText(Integer.toString(unopenedNotificationCount));
            buttonNotifications.setStyle("-fx-background-image: url('/images/not2.png')");
        }

    }

    private void setNotifications() {

        List<Notification> usersNotifications = notificationService.getUsersNotifications(loggedInUser.getId());
        userPage.setNotifications(usersNotifications);
        this.modelNotifications.setAll(usersNotifications);

    }

    private void setEvents() {

        ScheduledEvent event = listViewEvents.getSelectionModel().getSelectedItem();

        List<ScheduledEvent> events = eventService.getEventsOnPage(eventService.getEventsCurrentPage());
        this.modelEvents.setAll(events);

        listViewEvents.getSelectionModel().select(event);
        handleEventSelectionChanged(null);

    }

    private void setGroupChats() {

        int index = listViewGroupChats.getSelectionModel().getSelectedIndex();

        Iterable<List<User>> groupChats = messageService.getGroups(loggedInUser.getId());
        List<List<User>> groupChatsList = StreamSupport.stream(groupChats.spliterator(), false)
                .collect(Collectors.toList());
        userPage.setGroupChats(groupChatsList);
        this.modelGroupChats.setAll(groupChatsList);

        listViewGroupChats.scrollTo(index);
        listViewGroupChats.getSelectionModel().select(index);


    }

    private void setFindFriends() {

        Iterable<User> nonFriends = userService.getFindFriendsOnPage(userService.getFindFriendsCurrentPage(), loggedInUser.getId());
        List<User> nonFriendsList = StreamSupport.stream(nonFriends.spliterator(), false)
                .collect(Collectors.toList());
        userPage.setNonFriends(nonFriendsList);
        this.modelNonFriends.setAll(nonFriendsList);

    }

    private void setFriends() {

        Iterable<UserDateDto> friends = userService.getFriendsOnPage(userService.getFriendsCurrentPage(), loggedInUser.getId());
        List<UserDateDto> friendList = StreamSupport.stream(friends.spliterator(), false)
                .collect(Collectors.toList());
        userPage.setFriendsList(friendList);
        this.modelFriends.setAll(friendList);

    }

    private void setReceivedFriendRequests() {
        Iterable<UserFriendRequestDto> friendRequestsDto = friendRequestService
                .getReceivedFriendRequestsOnPage(friendRequestService.getReceivedFriendRequestsCurrentPage(), loggedInUser.getId());
        List<UserFriendRequestDto> friendRequests = StreamSupport.stream(friendRequestsDto.spliterator(), false)
                .collect(Collectors.toList());
        userPage.setReceivedFriendRequests(friendRequests);
        this.modelReceivedFriendRequests.setAll(friendRequests);
    }


    private void setSentFriendRequests() {
        Iterable<UserFriendRequestDto> friendRequestsDto = friendRequestService
                .getSentFriendRequestsOnPage(friendRequestService.getSentFriendRequestsCurrentPage(), loggedInUser.getId());
        List<UserFriendRequestDto> friendRequests = StreamSupport.stream(friendRequestsDto.spliterator(), false)
                .collect(Collectors.toList());
        userPage.setSentFriendRequests(friendRequests);
        this.modelSentFriendRequests.setAll(friendRequests);
    }

    private void setMessages(UserDateDto other) {

        vBoxChat.getChildren().removeAll(vBoxChat.getChildren());
        Iterable<Message> messages = messageService.getConversation(loggedInUser.getId(), other.getId());
        messages.forEach(message -> {
            Pane pane = new Pane();
            TextArea textArea = new TextArea();
            textArea.setPrefWidth(200);
            textArea.setPrefHeight(30);
            if(message.getRepliedMessage() == null) {
                textArea.setText(message.getText());
                if (message.getFrom_user().getId().equals(loggedInUser.getId()))
                    textArea.setLayoutX(35);
                else
                    textArea.setLayoutX(0);
            }
            else {
                Label repliedTo = new Label("Replied to: " + messageService.getOne(message.getRepliedMessage().getId()).getText());
                textArea.setText(message.getText()) ;
                if (message.getFrom_user().getId().equals(loggedInUser.getId())) {
                    repliedTo.setLayoutX(35);
                    textArea.setLayoutX(35);
                }
                else {
                    textArea.setLayoutX(0);
                    repliedTo.setLayoutX(0);
                }
                textArea.setLayoutY(20);
                pane.getChildren().add(repliedTo);
            }


            textArea.setEditable(false);
            textArea.setAccessibleText(message.getId().toString());
            textArea.setOnMouseClicked(x -> handleReplyMessage(textArea.getText(), textArea.getAccessibleText()));
            pane.getChildren().add(textArea);
            vBoxChat.getChildren().add(pane);
        });

        scrollPaneMessages.setVvalue(1.0);

    }

    private void setGroupMessages(List<User> friends) {

        vBoxChat.getChildren().removeAll(vBoxChat.getChildren());
        Iterable<Message> messages = messageService.getGroupChat(loggedInUser, friends);
        messages.forEach(message -> {
            Pane pane = new Pane();
            TextArea textArea = new TextArea();
            textArea.setPrefWidth(200);
            if (message.getFrom_user().getId().equals(loggedInUser.getId())) {
                if(message.getRepliedMessage() == null) {
                    textArea.setText(message.getText());
                }
                else {
                    Message repliedToMessage = messageService.getOne(message.getRepliedMessage().getId());
                    Label repliedTo = new Label("Replied to " + repliedToMessage.getFrom_user() + ": " + repliedToMessage.getText());
                    repliedTo.setLayoutX(35);
                    textArea.setLayoutY(20);
                    textArea.setText(message.getText());
                    pane.getChildren().add(repliedTo);
                }
                textArea.setPrefHeight(30);
                textArea.setLayoutX(35);
            }
            else {
                if(message.getRepliedMessage() == null) {
                    textArea.setText(message.getText());
                }
                else {
                    Message repliedToMessage = messageService.getOne(message.getRepliedMessage().getId());
                    Label repliedTo = new Label("Replied to " + repliedToMessage.getFrom_user() + ": " + repliedToMessage.getText());
                    repliedTo.setLayoutX(0);
                    textArea.setLayoutY(20);
                    textArea.setText(message.getText());
                    pane.getChildren().add(repliedTo);
                }
                textArea.setLayoutX(0);
                textArea.setPrefHeight(50);
                textArea.setText(message.getFrom_user() + ": \n" + message.getText());
            }
            textArea.setEditable(false);
            textArea.setAccessibleText(message.getId().toString());
            textArea.setOnMouseClicked(x -> handleReplyMessage(textArea.getText(), textArea.getAccessibleText()));
            textArea.setWrapText(true);
            pane.getChildren().add(textArea);
            vBoxChat.getChildren().add(pane);
        });

        scrollPaneMessages.setVvalue(1.0);

    }

    public void handleMousePressedOnBar(MouseEvent mousePressed) {
        this.mousePressed = mousePressed;
    }

    public void handleMouseDraggedBar(MouseEvent mouseEvent) {

        mainStage.setX(mouseEvent.getScreenX() - mousePressed.getSceneX());
        mainStage.setY(mouseEvent.getScreenY() - mousePressed.getSceneY());

    }

    public void handleSentRequests(ActionEvent actionEvent) {
        paneReceivedFriendRequests.setVisible(false);
        paneSentFriendRequests.setVisible(true);
    }

    public void handleReceivedRequests(ActionEvent actionEvent) {
        paneReceivedFriendRequests.setVisible(true);
        paneSentFriendRequests.setVisible(false);
    }

    public void handleLogOut(ActionEvent actionEvent) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/login.fxml"));
            AnchorPane root = loader.load();

            LoginController loginController = loader.getController();
            Stage loginStage = new Stage();
            loginStage.getIcons().add(new Image("/images/NanoPicNoBck.png"));
            loginController.setService(userService, friendshipService, friendRequestService, messageService, notificationService, eventService, loginStage);

            loginStage.setScene(new Scene(root));
            loginStage.initStyle(StageStyle.UNDECORATED);

            mainStage.close();
            loginStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void handleFriendRequestButton(ActionEvent actionEvent) {
        anchorPaneFindFriends.setVisible(false);
        anchorPaneProfile.setVisible(false);
        anchorPaneChat.setVisible(false);
        anchorPaneEvents.setVisible(false);
        anchorPaneFriendRequests.setVisible(true);
    }


    public void handleProfileButton(ActionEvent actionEvent) {
        anchorPaneFindFriends.setVisible(false);
        anchorPaneFriendRequests.setVisible(false);
        anchorPaneChat.setVisible(false);
        anchorPaneEvents.setVisible(false);
        anchorPaneProfile.setVisible(true);
    }

    public void handleRejectFriendRequest(ActionEvent actionEvent) {
        UserFriendRequestDto friendRequest = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
        if (friendRequest == null)
            labelReceivedFriendRequestError.setText("You must select an item");
        else {
            try {
                this.friendRequestService.rejectFriendRequest(loggedInUser.getId(), friendRequest.getId().getLeft());
                labelReceivedFriendRequestError.setText("Friend request rejected");
            } catch (ServiceException serviceException) {
                labelReceivedFriendRequestError.setText("Can't execute");
            }
        }
    }

    public void handleAcceptFriendRequest(ActionEvent actionEvent) {
        UserFriendRequestDto friendRequest = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
        if (friendRequest == null)
            labelReceivedFriendRequestError.setText("You must select an item");
        else {
            try {
                this.friendRequestService.acceptFriendRequest(loggedInUser.getId(), friendRequest.getId().getLeft());
                labelReceivedFriendRequestError.setText("Friend request accepted");
            } catch (ServiceException serviceException) {
                labelReceivedFriendRequestError.setText("Can't execute");
            }

        }
    }

    public void handleCancelSentFriendRequest(ActionEvent actionEvent) {
        UserFriendRequestDto friendRequest = tableViewSentFriendRequests.getSelectionModel().getSelectedItem();
        if (friendRequest == null)
            labelSentFriendRequestError.setText("You must select an item");
        else {
            try {
                this.friendRequestService.cancelFriendRequest(loggedInUser.getId(), friendRequest.getId().getRight());
                labelSentFriendRequestError.setText("Friend request canceled");
            } catch (ServiceException serviceException) {
                labelReceivedFriendRequestError.setText("Can't execute");
            }
        }
    }

    public void handleFindFriendButton(ActionEvent actionEvent) {
        anchorPaneProfile.setVisible(false);
        anchorPaneFriendRequests.setVisible(false);
        anchorPaneChat.setVisible(false);
        anchorPaneEvents.setVisible(false);
        anchorPaneFindFriends.setVisible(true);
    }

    private void handleFilterFindFriend() {

        User selected = listViewFindFriends.getSelectionModel().getSelectedItem();
        modelNonFriends.setAll(userService.getFilteredFindFriendsOnPage(userService.getFilteredFindFriendsCurrentPage()
                , loggedInUser.getId(), textFieldFindFriends.getText()));

        if(modelNonFriends.contains(selected))
            listViewFindFriends.getSelectionModel().select(selected);
        else
            listViewFindFriends.getSelectionModel().select(null);

    }

    private void handleFilterFriends() {

        UserDateDto selected = listViewFriends.getSelectionModel().getSelectedItem();
        modelFriends.setAll(userService.getFilteredFriendsOnPage(userService.getFilteredFriendsCurrentPage()
                , loggedInUser.getId(), textFieldSearchInFriends.getText()));

        if(modelFriends.contains(selected))
            listViewFriends.getSelectionModel().select(selected);
        else
            listViewFriends.getSelectionModel().select(null);

    }

    private void handleFilterEvents() {

        modelEvents.setAll(eventService.getEvents().stream()
                .filter(x -> x.getName().toLowerCase().contains(textFieldSearchEvents.getText().toLowerCase())).collect(Collectors.toList()));

    }

    public void handleSelectionChanged(MouseEvent mouseEvent) {

        User other_user = listViewFindFriends.getSelectionModel().getSelectedItem();
        if (other_user == null)
            return;
        paneOtherUserProfile.setVisible(true);
        imageFindFriendsProfile.setVisible(true);
        labelOtherUserProfileUsername.setText(other_user.getFirstName() + " " + other_user.getLastName());
        FriendRequest friendRequest = friendRequestService.getOne(loggedInUser.getId(), other_user.getId());

        if (friendRequest != null && friendRequest.getStatus().equals("pending")) {

            buttonSendFriendRequest.setText("Cancel request");
            buttonSendFriendRequest.setVisible(true);
            buttonFindFriendAcceptRequest.setVisible(false);
            buttonFindFriendRejectRequest.setVisible(false);
            labelFindFriendsRequestStatus.setText("You sent a friend request to this user");

        } else if (friendRequest == null || !friendRequest.getStatus().equals("pending")) {

            friendRequest = friendRequestService.getOne(other_user.getId(), loggedInUser.getId());
            if (friendRequest != null && friendRequest.getStatus().equals("pending")) {
                labelFindFriendsRequestStatus.setText("Respond to friend request");
                buttonSendFriendRequest.setVisible(false);
                buttonFindFriendAcceptRequest.setVisible(true);
                buttonFindFriendRejectRequest.setVisible(true);
            } else if (friendRequest == null || !friendRequest.getStatus().equals("pending")) {
                buttonSendFriendRequest.setVisible(true);
                buttonFindFriendAcceptRequest.setVisible(false);
                buttonFindFriendRejectRequest.setVisible(false);
                labelFindFriendsRequestStatus.setText("Not friends yet?");
                buttonSendFriendRequest.setText("Add friend");
            }

        } else {
            buttonSendFriendRequest.setVisible(true);
            buttonFindFriendAcceptRequest.setVisible(false);
            buttonFindFriendRejectRequest.setVisible(false);
            labelFindFriendsRequestStatus.setText("Not friends yet?");
            buttonSendFriendRequest.setText("Add friend");
        }

    }

    public void handleSendFriendRequest(ActionEvent actionEvent) {

        User selectedUser = listViewFindFriends.getSelectionModel().getSelectedItem();
        if (selectedUser == null)
            return;

        try {

            if (buttonSendFriendRequest.getText().equals("Add friend")) {

                notificationService.add(new Notification(selectedUser.getId()
                        , loggedInUser.getFirstName() + " " + loggedInUser.getLastName() + " sent you a friend request"
                        , "FriendRequest", false, LocalDateTime.now()));
                friendRequestService.sendFriendRequest(loggedInUser.getId(), selectedUser.getId());
                buttonSendFriendRequest.setText("Cancel request");
                labelFindFriendsRequestStatus.setText("You sent a friend request to this user");

            } else {

                friendRequestService.cancelFriendRequest(loggedInUser.getId(), selectedUser.getId());
                buttonSendFriendRequest.setText("Add friend");
                labelFindFriendsRequestStatus.setText("Not friends yet?");

            }

        } catch (ServiceException | RepoException exception) {
            MessageAlert.showErrorMessage(null, "Something went wrong");
        }

    }


    public void handleFindFriendAcceptRequest(ActionEvent actionEvent) {

        User selectedUser = listViewFindFriends.getSelectionModel().getSelectedItem();
        if (selectedUser == null)
            return;
        else {
            try {
                friendRequestService.acceptFriendRequest(loggedInUser.getId(), selectedUser.getId());
                buttonSendFriendRequest.setText("Cancel request");
                buttonSendFriendRequest.setVisible(false);
                buttonFindFriendAcceptRequest.setVisible(false);
                buttonFindFriendRejectRequest.setVisible(false);
                imageFindFriendsProfile.setVisible(false);
                labelFindFriendsRequestStatus.setText("");
                labelOtherUserProfileUsername.setText("");
            } catch (ServiceException | RepoException exception) {
                MessageAlert.showErrorMessage(null, "Something went wrong!");
            }
        }

    }

    public void handleFindFriendRejectRequeste(ActionEvent actionEvent) {

        User selectedUser = listViewFindFriends.getSelectionModel().getSelectedItem();
        if (selectedUser == null)
            return;
        else {
            try {
                friendRequestService.rejectFriendRequest(loggedInUser.getId(), selectedUser.getId());
                buttonSendFriendRequest.setText("Add friend");
                buttonSendFriendRequest.setVisible(true);
                buttonFindFriendAcceptRequest.setVisible(false);
                buttonFindFriendRejectRequest.setVisible(false);
                labelFindFriendsRequestStatus.setText("Not friends yet?");
            } catch (ServiceException | RepoException exception) {
                MessageAlert.showErrorMessage(null, "Something went wrong!");
            }
        }

    }

    public void handleMouseClickedFriendsListItem(MouseEvent mouseEvent) {

        UserDateDto friend = listViewFriends.getSelectionModel().getSelectedItem();
        if (friend == null)
            return;

        this.currentFriend = friend;

        if (!anchorPaneNewGroup.isVisible()) {

            if (anchorPaneChat.isVisible())
                anchorPaneChatProfile.setVisible(true);
            labelChatProfileUsername.setText(friend.getFirstName() + " " + friend.getLastName());
            setMessages(friend);
            scrollPaneMessages.setVisible(true);
            textFieldTypeMessage.setVisible(true);
            buttonSendMessage.setVisible(true);
            anchorPaneGroupChat.setVisible(false);
            labelChatUsername.setText(friend.getFirstName() + " " + friend.getLastName());
            chatReceiver = "user";
            scrollPaneMessages.setVvalue(1.0);

        } else {

            UserDateDto selectedFriend = listViewFriends.getSelectionModel().getSelectedItem();
            if (!newGroupMembersList.contains(selectedFriend)) {
                newGroupMembersList.add(selectedFriend);
                this.modelNewGroupMembers.setAll(newGroupMembersList);
                listViewNewGroupMembers.setItems(modelNewGroupMembers);

            }

        }

        if (anchorPaneActivityLog.isVisible()) {
            radioButtonFriendMessages.setText(friend.getFirstName() + " " + friend.getLastName());
        }

        labelReplyTo.setText("");
        selectedIdToReply = (long)0;
        scrollPaneMessages.setVvalue(1.0);

    }

    public void handleOpenChatButton(ActionEvent actionEvent) {
        anchorPaneProfile.setVisible(false);
        anchorPaneFriendRequests.setVisible(false);
        anchorPaneFindFriends.setVisible(false);
        anchorPaneEvents.setVisible(false);
        anchorPaneChat.setVisible(true);
    }

    public void handleSendMessage(ActionEvent actionEvent) {

        if (textFieldTypeMessage.getText().equals(""))
            return;

        if (currentFriend == null && currentGroup == null)
            return;

        Pane pane = new Pane();
        TextArea textArea;

        if(selectedIdToReply.equals((long)0)) {
            textArea = new TextArea(textFieldTypeMessage.getText());
        } else {
            Label repliedTo = new Label(labelReplyTo.getText());
            repliedTo.setLayoutX(35);
            textArea = new TextArea(textFieldTypeMessage.getText());
            textArea.setLayoutY(20);
            pane.getChildren().add(repliedTo);
        }

        textArea.setPrefHeight(30);
        textArea.setPrefWidth(200);
        textArea.setLayoutX(35);
        textArea.setEditable(false);
        pane.getChildren().add(textArea);
        vBoxChat.getChildren().add(pane);

        try {
            boolean isGroup = false;
            List<Long> to_users = new ArrayList<>();
            if (chatReceiver.equals("user")) {
                to_users.add(currentFriend.getId());
            } else if (chatReceiver.equals("group")) {
                isGroup = true;
                to_users = currentGroup
                        .stream()
                        .map(Entity::getId)
                        .collect(Collectors.toList());
                to_users.remove(loggedInUser.getId());
            }
            if(selectedIdToReply.equals((long)0))
                messageService.sendMessage(loggedInUser.getId(), to_users, textFieldTypeMessage.getText(), isGroup);
            else {
                messageService.replyToAll(loggedInUser.getId(), selectedIdToReply, textFieldTypeMessage.getText(), isGroup);
                selectedIdToReply = (long)0;
            }
        } catch (ServiceException | ValidationException exception) {
            MessageAlert.showErrorMessage(null, "Something went wrong");
        }

        labelReplyTo.setText("");
        textFieldTypeMessage.setText("");
        selectedIdToReply = (long)0;
        //scrollPaneMessages.setVvalue(1.0);

    }

    public void handleMessageKeyPress(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
            handleSendMessage(null);
    }

    public void handleRemoveFriend(ActionEvent actionEvent) {

        UserDateDto friend = listViewFriends.getSelectionModel().getSelectedItem();
        try {
            friendshipService.delete(new Tuple<>(loggedInUser.getId(), friend.getId()));
            scrollPaneMessages.setVisible(false);
        } catch (ServiceException exception) {
            MessageAlert.showErrorMessage(null, "Something went wrong");
        }

    }

    public void handleMouseExitedProfileCloser(MouseEvent mouseEvent) {
        anchorPaneChatProfile.setVisible(false);
        anchorPaneGroupChat.setVisible(true);
    }


    public void handleMouseEnteredProfileOpener(MouseEvent mouseEvent) {

        if (listViewFriends.getSelectionModel().getSelectedItem() != null && anchorPaneChat.isVisible()) {
            anchorPaneChatProfile.setVisible(true);
            anchorPaneGroupChat.setVisible(false);
        }

    }

    public void handleNewGroupChat(ActionEvent actionEvent) {

        scrollPaneMessages.setVisible(false);
        textFieldTypeMessage.setVisible(false);
        buttonSendMessage.setVisible(false);
        labelChatUsername.setVisible(false);
        anchorPaneNewGroup.setVisible(true);

    }

    public void exitNewGroupPane(ActionEvent actionEvent) {

        anchorPaneNewGroup.setVisible(false);
        scrollPaneMessages.setVisible(true);
        textFieldTypeMessage.setVisible(true);
        buttonSendMessage.setVisible(true);
        labelChatUsername.setVisible(true);
        newGroupMembersList = new ArrayList<>();
        this.modelNewGroupMembers.setAll(newGroupMembersList);

    }

    public void handleMouseClickRemoveFromNewGroup(MouseEvent mouseEvent) {

        UserDateDto selectedMember = listViewNewGroupMembers.getSelectionModel().getSelectedItem();
        if (selectedMember == null)
            return;
        newGroupMembersList.remove(selectedMember);
        this.modelNewGroupMembers.setAll(newGroupMembersList);

    }

    public void handleDoneNewGroup(ActionEvent actionEvent) {

        Iterable<List<User>> groupChats = messageService.getGroups(loggedInUser.getId());
        List<List<User>> groupChatsList = StreamSupport.stream(groupChats.spliterator(), false)
                .collect(Collectors.toList());
        groupChatsList.add(newGroupMembersList
                .stream()
                .map(x -> new User(x.getFirstName(), x.getLastName(), x.getId()))
                .collect(Collectors.toList())
        );


        this.modelGroupChats.setAll(groupChatsList);
        exitNewGroupPane(null);

    }


    public void handleOpenGroupChat(MouseEvent mouseEvent) {

        List<User> friends = new ArrayList<>(listViewGroupChats.getSelectionModel().getSelectedItem());
        if (friends.size() == 0)
            return;

        this.currentGroup = friends;

        scrollPaneMessages.setVisible(true);
        textFieldTypeMessage.setVisible(true);
        buttonSendMessage.setVisible(true);

        chatReceiver = "group";
        labelChatUsername.setText(listViewGroupChats.getSelectionModel().getSelectedItem().toString());
        setGroupMessages(currentGroup);
        labelReplyTo.setText("");

    }


    private void handleReplyMessage(String text, String idStr) {

        text = text.replace("\n", "");
        if(chatReceiver.equals("group")) {
            labelReplyTo.setText("Reply to " + text);
            selectedIdToReply = Long.parseLong(idStr);
        }
        else {
            labelReplyTo.setText("Reply to: " + text);
            selectedIdToReply = Long.parseLong(idStr);
        }
        labelReplyTo.setVisible(true);

    }


    public void handleMouseClickedOnLabelReplyTo(MouseEvent mouseEvent) {
        labelReplyTo.setText("");
        selectedIdToReply = (long)0;
    }

    public void handleOpenActivityLogPane(ActionEvent actionEvent) {
        buttonChat.setVisible(false);
        buttonFindFriends.setVisible(false);
        buttonFriendRequests.setVisible(false);
        buttonProfile.setVisible(false);
        buttonEvents.setVisible(false);
        anchorPaneProfile.setVisible(false);
        anchorPaneActivityLog.setVisible(true);
    }

    public void handleCancelActivityLog(ActionEvent actionEvent) {
        anchorPaneActivityLog.setVisible(false);
        buttonChat.setVisible(true);
        buttonFindFriends.setVisible(true);
        buttonFriendRequests.setVisible(true);
        buttonProfile.setVisible(true);
        buttonEvents.setVisible(true);
        anchorPaneProfile.setVisible(true);
    }

    public void handleSelectedFriendships(ActionEvent actionEvent) {
        radioButtonFriendMessages.setSelected(false);
    }

    public void handleSelectedMessages(ActionEvent actionEvent) {
        radioButtonFriendMessages.setSelected(false);
    }

    public void handleSelectedFriendMessages(ActionEvent actionEvent) {
        radioButtonMessages.setSelected(false);
        radioButtonFriendships.setSelected(false);
    }

    public void handleGenerateActivityLog(ActionEvent actionEvent) {

        labelGenerateActivityLogError.setText("");
        if (datePickerActivityLogFrom.getValue() == null || datePickerActivityLogTo.getValue() == null)
            labelGenerateActivityLogError.setText("You must pick the date interval");
        if (!radioButtonFriendMessages.isSelected() && !radioButtonFriendships.isSelected() && !radioButtonMessages.isSelected()) {
            if (!labelGenerateActivityLogError.getText().equals(""))
                labelGenerateActivityLogError.setText(labelGenerateActivityLogError.getText() + "\n");
            labelGenerateActivityLogError.setText(labelGenerateActivityLogError.getText() + "You need to pick an option");
        }

        if (!labelGenerateActivityLogError.getText().equals(""))
            return;

        LocalDate logDateFrom = datePickerActivityLogFrom.getValue();
        LocalDate logDateTo = datePickerActivityLogTo.getValue();

        List<String> activityLogString = new ArrayList<>();

        if (radioButtonFriendships.isSelected()) {

            activityLogString.add("Friendship Log:");

            activityLogFriendships = userService.getFriendshipsBetweenDates(loggedInUser, logDateFrom, logDateTo);
            activityLogFriendships.forEach(x -> activityLogString.add(x.getStringForLog()));
            activityLogString.add("");

        }

        if (radioButtonMessages.isSelected()) {

            activityLogString.add("Message Log:");

            activityLogMessages = messageService.getMessagesBetweenDates(loggedInUser, logDateFrom, logDateTo);
            activityLogMessages.forEach(x -> activityLogString.add(x.toString()));

        }

        if (radioButtonFriendMessages.isSelected()) {

            UserDateDto friend = listViewFriends.getSelectionModel().getSelectedItem();
            if (friend == null) {
                labelGenerateActivityLogError.setText("You must select a friend");
                return;
            }

            activityLogString.add("Message log from " + friend + ":");
            activityLogMessages = messageService.getMessagesFromFriendBetweenDates(loggedInUser
                    , new User(friend.getFirstName(), friend.getLastName(), friend.getId()), logDateFrom, logDateTo);
            activityLogMessages.forEach(x -> activityLogString.add(x.toString().replace(friend.toString() + ": ", "")));

        }

        this.modelActivityLog.setAll(activityLogString);
        listViewActivityLog.setItems(modelActivityLog);

    }

    public void handleGenerateActivityLogPDF(ActionEvent actionEvent) {

        String fileName = textFieldFileNameActivityLog.getText();


        if (listViewActivityLog.getItems().size() == 0) {
            labelGenerateActivityLogError.setText("You must complete the preview first");
            return;
        }

        if (fileName.equals("")) {
            labelGenerateActivityLogError.setText("You must enter the file name");
            return;
        }

        UserActivityLog userActivityLog = new UserActivityLog(loggedInUser, datePickerActivityLogFrom.getValue()
                , datePickerActivityLogTo.getValue(), fileName);

        List<String> stringList = listViewActivityLog.getItems();
        String message = "";
        try {
            userActivityLog.createPdf(stringList);
            message = "Success! Your activity log\nis located on Desktop";
            listViewActivityLog.setDisable(true);
        } catch (IOException e) {
            e.printStackTrace();
            message = "Something went wrong!";
        }

        this.modelActivityLog.removeAll();
        listViewActivityLog.setItems(modelActivityLog);
        labelDonePDFMessage.setText(message);
        paneDonePDF.setVisible(true);

    }

    public void buttonCloseDonePDFPane(ActionEvent actionEvent) {

        paneDonePDF.setVisible(false);
        listViewActivityLog.setDisable(false);

    }

    public void handleOpenNotifications(ActionEvent actionEvent) {

        if(listViewNotifications.isVisible()) {
            listViewNotifications.setVisible(false);
            List<Notification> notifications = new ArrayList<>(modelNotifications);
            notifications.forEach(x -> x.setText(x.getText().replace("*", "")));
            modelNotifications.setAll(notifications);

        }
        else {

            listViewNotifications.setVisible(true);
            notifications.forEach(x -> x.setOpened(true));
            labelNotificationsCount.setVisible(false);
            buttonNotifications.setStyle("-fx-background-image: url('/images/not1.png')");
            notificationService.openUsersNotifications(notifications);
            //notifications = notificationService.getUsersNotifications(loggedInUser.getId());

        }

    }

    public void handleOpenEventsPane(ActionEvent actionEvent) {

        anchorPaneProfile.setVisible(false);
        anchorPaneFriendRequests.setVisible(false);
        anchorPaneFindFriends.setVisible(false);
        anchorPaneChat.setVisible(false);
        anchorPaneEvents.setVisible(true);

    }

    public void handleCreateEvent(ActionEvent actionEvent) {

        if(textFieldEventName.getText().equals("") || textFieldEventLocation.getText().equals("") ||
                datePickerEventDate.getValue() == null || textFiledEventHour.getText().equals(" ") ||
                textFieldEventMinute.getText().equals("") || textAreaEventDescription.getText().equals("")) {

            labelEventCreateError.setText("All fields must be completed");
            return;

        }

        try{

            long hour = Long.parseLong(textFiledEventHour.getText());
            long minutes = Long.parseLong(textFieldEventMinute.getText());

            if(hour < 0 || hour > 24 || minutes < 0 || minutes > 60) {
                labelEventCreateError.setText("Invalid time");
                return;
            }

        } catch (NumberFormatException exp) {
            labelEventCreateError.setText("Invalid time");
            return;
        }

        String hour = textFiledEventHour.getText(), minutes = textFieldEventMinute.getText();
        if(hour.length() == 1)
            hour = "0" + hour;
        if(minutes.length() == 1)
            minutes = "0" + minutes;

        String datetimeString = datePickerEventDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " "
                + hour + ":" + minutes;

        ScheduledEvent event = new ScheduledEvent(textFieldEventName.getText(), textAreaEventDescription.getText()
                , textFieldEventLocation.getText(), LocalDateTime.parse(datetimeString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
                , new ArrayList<>(Collections.singletonList(loggedInUser)));

        eventService.addEvent(event);

        paneCreateEvent.setVisible(false);
        paneEventInfo.setVisible(true);
        textAreaEventDescription.setText("");
        textFieldEventName.setText("");
        textFieldEventLocation.setText("");
        datePickerEventDate.getEditor().setText("");
        textFiledEventHour.setText("");
        textFieldEventMinute.setText("");
        labelEventCreateError.setText("");

    }

    public void handleCancelCreateEvent(ActionEvent actionEvent) {

        paneCreateEvent.setVisible(false);
        paneEventInfo.setVisible(true);
        textAreaEventDescription.setText("");
        textFieldEventName.setText("");
        textFieldEventLocation.setText("");
        datePickerEventDate.getEditor().setText("");
        textFiledEventHour.setText("");
        textFieldEventMinute.setText("");
        labelEventCreateError.setText("");

    }

    public void handleOpenCreateEventPane(ActionEvent actionEvent) {

        paneEventInfo.setVisible(false);
        paneCreateEvent.setVisible(true);

    }

    public void handleEventSelectionChanged(MouseEvent mouseEvent) {

        ScheduledEvent event = listViewEvents.getSelectionModel().getSelectedItem();
        if(event == null)
            return;

        labelEventInfoName.setText(event.getName());
        labelEventInfoLocation.setText(event.getLocation());
        labelEventInfoDate.setText(event.getDatetime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        textAreaEventInfoDescription.setText(event.getDescription());
        this.modelEventJoined.setAll(event.getJoinedUsers());

        if(event.getJoinedUsers().contains(loggedInUser)) {
            buttonJoinEvent.setText("Leave event");

            if(eventService.getOne(new Tuple<>(event.getId(), loggedInUser.getId())).isNotificationsOn())
                buttonNotificationsSettingsEvent.setStyle("-fx-background-image: url('/images/notOffDim.png')");
            else
                buttonNotificationsSettingsEvent.setStyle("-fx-background-image: url('/images/notOnDim.png')");

            buttonNotificationsSettingsEvent.setVisible(true);

        }
        else {
            buttonNotificationsSettingsEvent.setVisible(false);
            buttonJoinEvent.setText("Join event");
        }

        buttonJoinEvent.setVisible(true);

    }

    public void handleJoinOrLeaveEvent(ActionEvent actionEvent) {

        ScheduledEvent event = listViewEvents.getSelectionModel().getSelectedItem();
        if(event == null)
            return;

        Participant participant = eventService.updateJoiningStatus(new Tuple<>(event.getId(), loggedInUser.getId()));
        if(participant != null) {

            String days = setDaysToEvent(LocalDateTime.now(), event.getDatetime());
            if(!days.equals("")) {
                Notification notification = new Notification(loggedInUser.getId(), event.getName() + " event happening in less than"
                        + days, "Event", false, LocalDateTime.now());
                notificationService.add(notification);
            }

        }

        handleEventSelectionChanged(null);

    }

    public void handleTurnOnnOffEventNotifications(ActionEvent actionEvent) {

        ScheduledEvent event = listViewEvents.getSelectionModel().getSelectedItem();
        if(event == null)
            return;

        eventService.turnNotificationsOnOff(new Participant(new Tuple<>(event.getId(), loggedInUser.getId())));

    }

    public void handleMouseClickedNotification(MouseEvent mouseEvent) {

        Notification notification = listViewNotifications.getSelectionModel().getSelectedItem();
        if(notification == null)
            return;

        String type = notification.getType();

        if(type.equals("Event")) {
            listViewNotifications.setVisible(false);
            handleOpenEventsPane(null);
        }

        else if(type.equals("FriendRequest")) {
            listViewNotifications.setVisible(false);
            handleFriendRequestButton(null);
            handleReceivedRequests(null);
        }

        listViewNotifications.getSelectionModel().select(null);

    }

    User nonFriend = null;
    public void handleScrollFindFriendsList(ScrollEvent scrollEvent) {

        User selected = listViewFindFriends.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            if(textFieldFindFriends.getText().equals(""))
                modelNonFriends.setAll(userService.getPreviousFindFriends(loggedInUser.getId()));
            else
                modelNonFriends.setAll(userService.getPreviousFilteredFindFriends(loggedInUser.getId(), textFieldFindFriends.getText()));
        } else {
            if(textFieldFindFriends.getText().equals(""))
                modelNonFriends.setAll(userService.getNextFindFriends(loggedInUser.getId()));
            else
                modelNonFriends.setAll(userService.getNextFilteredFindFriends(loggedInUser.getId(), textFieldFindFriends.getText()));
        }

        if(selected != null)
            nonFriend = selected;

        selected = listViewFindFriends.getSelectionModel().getSelectedItem();

        if(selected == null) {
            if(modelNonFriends.contains(nonFriend))
                listViewFindFriends.getSelectionModel().select(nonFriend);
            else
                listViewFindFriends.getSelectionModel().select(null);
        }
        else {
            nonFriend = selected;
            listViewFindFriends.getSelectionModel().select(nonFriend);
        }

    }

    UserDateDto friend = null;
    public void handleScrollFriendsList(ScrollEvent scrollEvent) {

        UserDateDto selected = listViewFriends.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            if(textFieldSearchInFriends.getText().equals(""))
                modelFriends.setAll(userService.getPreviousFriends(loggedInUser.getId()));
            else
                modelFriends.setAll(userService.getPreviousFilteredFriends(loggedInUser.getId(), textFieldSearchInFriends.getText()));
        } else {
            if(textFieldSearchInFriends.getText().equals(""))
                modelFriends.setAll(userService.getNextFriends(loggedInUser.getId()));
            else
                modelFriends.setAll(userService.getNextFilteredFriends(loggedInUser.getId(), textFieldSearchInFriends.getText()));
        }

        if(selected != null)
            friend = selected;

        selected = listViewFriends.getSelectionModel().getSelectedItem();

        if(selected == null) {
            if(modelFriends.contains(friend))
                listViewFriends.getSelectionModel().select(friend);
            else
                listViewFriends.getSelectionModel().select(null);
        }
        else {
            friend = selected;
            listViewFriends.getSelectionModel().select(friend);
        }


    }

    UserFriendRequestDto received = null;
    public void handleScrollReceivedFriendRequests(ScrollEvent scrollEvent) {

        UserFriendRequestDto selected = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            modelReceivedFriendRequests.setAll(friendRequestService.getPreviousReceivedFriendRequests(loggedInUser.getId()));
        } else {
            modelReceivedFriendRequests.setAll(friendRequestService.getNextReceivedFriendRequests(loggedInUser.getId()));
        }

        if(selected != null)
            received = selected;

        selected = tableViewReceivedRequests.getSelectionModel().getSelectedItem();

        if(selected == null) {
            if(modelReceivedFriendRequests.contains(received))
                tableViewReceivedRequests.getSelectionModel().select(received);
            else
                tableViewReceivedRequests.getSelectionModel().select(null);
        }
        else {
            received = selected;
            tableViewReceivedRequests.getSelectionModel().select(received);
        }

    }

    UserFriendRequestDto sent = null;
    public void handleScrollSentFriendRequests(ScrollEvent scrollEvent) {

        UserFriendRequestDto selected = tableViewSentFriendRequests.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            modelSentFriendRequests.setAll(friendRequestService.getPreviousSentFriendRequests(loggedInUser.getId()));
        } else {
            modelSentFriendRequests.setAll(friendRequestService.getNextSentFriendRequests(loggedInUser.getId()));
        }

        if(selected != null)
            sent = selected;

        selected = tableViewSentFriendRequests.getSelectionModel().getSelectedItem();

        if(selected == null) {
            if(modelSentFriendRequests.contains(sent))
                tableViewSentFriendRequests.getSelectionModel().select(sent);
            else
                tableViewSentFriendRequests.getSelectionModel().select(null);
        }
        else {
            sent = selected;
            tableViewSentFriendRequests.getSelectionModel().select(sent);
        }

    }

    ScheduledEvent event = null;
    public void handleScrollEvents(ScrollEvent scrollEvent) {

        ScheduledEvent selected = listViewEvents.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            modelEvents.setAll(eventService.getPreviousEventsOnPage());
        } else {
            modelEvents.setAll(eventService.getNextEventsOnPage());
        }

        if(selected != null)
            event = selected;

        selected = listViewEvents.getSelectionModel().getSelectedItem();

        if(selected == null) {
            if(modelEvents.contains(event))
                listViewEvents.getSelectionModel().select(event);
            else
                listViewEvents.getSelectionModel().select(null);
        }
        else {
            event = selected;
            listViewEvents.getSelectionModel().select(event);
        }

    }

    public void handleMouseEnteredNanoPic(MouseEvent mouseEvent) {

        paneNanoPic.setStyle("-fx-background-color: #cc9900");
        labelNanoPic.setStyle("-fx-text-fill: #000000");

    }

    public void handleMouseExitedNanoPic(MouseEvent mouseEvent) {

        paneNanoPic.setStyle("-fx-background-color: transparent");
        labelNanoPic.setStyle("-fx-text-fill: #cc9900");

    }
}
