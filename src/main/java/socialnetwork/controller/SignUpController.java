package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.User;
import socialnetwork.exceptions.RepoException;
import socialnetwork.service.UserDbService;

public class SignUpController {
    public TextField textFieldSignUpFirstName;
    public TextField textFieldSignUpLastName;
    public PasswordField textFieldSignUpPassword;
    public Label labelGeneratedUID;
    public Label newAccInfoLabel;
    public PasswordField textFieldSignUpNickname;
    private UserDbService userService;
    private Stage signUpStage;
    private MouseEvent mousePressed;

    public void setService(UserDbService userService, Stage signUpStage) {
        this.userService = userService;
        this.signUpStage = signUpStage;
    }

    public void handleCreateAccount(ActionEvent actionEvent) {

        if(textFieldSignUpFirstName.getText().equals("") || textFieldSignUpLastName.getText().equals("")
                || textFieldSignUpPassword.getText().equals("")) {
            newAccInfoLabel.setText("All fields must be filled");
            return;
        }

        User new_user = new User(textFieldSignUpFirstName.getText(), textFieldSignUpLastName.getText()
                , textFieldSignUpPassword.getText(), textFieldSignUpNickname.getText());

        try {
            new_user = userService.add(new_user);
        } catch (RepoException re) {
            newAccInfoLabel.setText(re.getMessage());
            return;
        }

        labelGeneratedUID.setText(new_user.getId().toString());
        newAccInfoLabel.setText("This is your unique UID. Don't forget it!\nAccount created. Go back to login!");

    }

    public void handleSignUpClose(ActionEvent actionEvent) {
        signUpStage.close();
    }

    public void handleMouseDraggedBar(MouseEvent mouseEvent) {
        signUpStage.setX(mouseEvent.getScreenX() - mousePressed.getSceneX());
        signUpStage.setY(mouseEvent.getScreenY() - mousePressed.getSceneY());
    }

    public void handleMousePressOnBar(MouseEvent mousePressed) {
        this.mousePressed = mousePressed;
    }
}
