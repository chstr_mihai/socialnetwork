package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.User;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.*;

import java.io.IOException;

public class LoginController {
    public Label labelLoginError;
    public Button buttonLogin;
    public Hyperlink hyperLinkForgotPassword;
    public Label labelSignInForgotPassword;
    public PasswordField textFieldNickname;
    public Button buttonSubmitForgotPassword;
    public Button buttonCancelForgotPassword;
    public Label labelErrorResetPassword;
    private Stage loginStage;
    private Stage signUpStage;
    private UserDbService userService;
    private FriendshipDbService friendshipService;
    private FriendRequestDbService friendRequestService;
    private MessageDbService messageService;
    private NotificationDbService notificationService;
    private EventDbService eventService;

    @FXML
    private TextField textFieldId;
    @FXML
    private PasswordField textFieldPassword;
    private MouseEvent mousePressed;

    public void setService(UserDbService userService, FriendshipDbService friendshipService
            , FriendRequestDbService friendRequestService, MessageDbService messageService
            , NotificationDbService notificationService, EventDbService eventService, Stage stage) {

        this.loginStage = stage;
        this.signUpStage = new Stage();
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.friendRequestService = friendRequestService;
        this.messageService = messageService;
        this.notificationService = notificationService;
        this.eventService = eventService;

    }

    private User validateInput() {

        if (textFieldId.getText().equals("") || textFieldPassword.getText().equals("")) {
            MessageAlert.showErrorMessage(null, "You need to complete both text fields");
            return null;
        }

        User user = null;
        try {
            user = userService.getUser(Long.parseLong(textFieldId.getText()));
        } catch (ServiceException | IllegalArgumentException exception) {
            MessageAlert.showErrorMessage(null, "Incorrect userid or password");
            return null;
        }

        return user;

    }

    public void handleLogin(ActionEvent actionEvent) {
        try {

            User loggedIn = userService.login(Long.parseLong(textFieldId.getText()), textFieldPassword.getText());

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/mainPage.fxml"));
            AnchorPane root = loader.load();

            MainController mainController = loader.getController();
            Stage mainStage = new Stage();
            mainStage.getIcons().add(new Image("/images/NanoPicNoBck.png"));


            Page userPage = new Page(loggedIn.getFirstName(), loggedIn.getLastName(), userService.getFriendsOnPage(0, loggedIn.getId())
                    , messageService.getGroups(loggedIn.getId()), userService.getFindFriendsOnPage(0, loggedIn.getId())
                    , friendRequestService.getReceivedFriendRequestsOnPage(0, loggedIn.getId())
                    , friendRequestService.getSentFriendRequestsOnPage(0, loggedIn.getId())
                    , notificationService.getUsersNotifications(loggedIn.getId()), eventService.getEventsOnPage(0));

            mainController.setEnvironment(userService, friendshipService, friendRequestService, messageService
                    , notificationService, eventService, mainStage, loggedIn, userPage);

            mainStage.setScene(new Scene(root));
            mainStage.initStyle(StageStyle.UNDECORATED);
            mainStage.setTitle("NanoPic");
            loginStage.close();
            mainStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServiceException | IllegalArgumentException exception) {
            labelLoginError.setText("Invalid id or password");
        }

    }

    public void handleSignUp(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/signup.fxml"));
            AnchorPane root = loader.load();

            SignUpController signUpController = loader.getController();
            Stage signUpStage = new Stage();
            signUpStage.getIcons().add(new Image("/images/NanoPicNoBck.png"));
            signUpController.setService(userService, signUpStage);

            signUpStage.setScene(new Scene(root));
            signUpStage.initStyle(StageStyle.UNDECORATED);
            signUpStage.setTitle("Sign Up");
            signUpStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void handleMousePressedOnBar(MouseEvent mousePressed) {
        this.mousePressed = mousePressed;
    }

    public void handleMouseDraggedBar(MouseEvent mouseEvent) {

        loginStage.setX(mouseEvent.getScreenX() - mousePressed.getSceneX());
        loginStage.setY(mouseEvent.getScreenY() - mousePressed.getSceneY());

    }

    public void handleCloseApp(ActionEvent actionEvent) {
        loginStage.close();
    }

    public void handleLogInKeyPress(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER) && labelSignInForgotPassword.getText().equals("Sign In"))
            handleLogin(null);
    }

    public void handleForgotPassword(ActionEvent actionEvent) {

        labelSignInForgotPassword.setText("Reset Password");
        hyperLinkForgotPassword.setVisible(false);
        buttonLogin.setVisible(false);
        textFieldNickname.setVisible(true);
        buttonSubmitForgotPassword.setVisible(true);
        buttonCancelForgotPassword.setVisible(true);
        labelLoginError.setText("");
        textFieldPassword.setPromptText("New Password");
        textFieldPassword.setText("");
        textFieldId.setText("");
        textFieldNickname.setText("");

    }

    public void handleSubmitForgotPassword(ActionEvent actionEvent) {

        if(textFieldId.getText().equals("") || textFieldPassword.getText().equals("") || textFieldNickname.getText().equals("")) {
            labelErrorResetPassword.setText("All fields must be completed");
            return;
        }

        try{
            userService.resetPassword(Long.parseLong(textFieldId.getText()), textFieldNickname.getText()
                    ,textFieldPassword.getText());
        } catch(ServiceException serviceException) {
            labelErrorResetPassword.setText(serviceException.getMessage());
            return;
        }

        labelErrorResetPassword.setText("Password submitted. Go back to login!");

    }

    public void handleCancelForgotPassword(ActionEvent actionEvent) {

        labelSignInForgotPassword.setText("Sign In");
        labelErrorResetPassword.setText("");
        textFieldNickname.setVisible(false);
        buttonSubmitForgotPassword.setVisible(false);
        buttonCancelForgotPassword.setVisible(false);
        hyperLinkForgotPassword.setVisible(true);
        buttonLogin.setVisible(true);
        textFieldPassword.setPromptText("Password");
        textFieldPassword.setText("");
        textFieldId.setText("");
        textFieldNickname.setText("");

    }
}
