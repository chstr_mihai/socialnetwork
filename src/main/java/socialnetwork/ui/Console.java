package socialnetwork.ui;

import socialnetwork.domain.*;
import socialnetwork.exceptions.ValidationException;
import socialnetwork.exceptions.RepoException;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.FriendRequestDbService;
import socialnetwork.service.FriendshipDbService;
import socialnetwork.service.MessageDbService;
import socialnetwork.service.UserDbService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Console {
    UserDbService userService;
    FriendshipDbService friendshipService;
    FriendRequestDbService friendRequestService;
    MessageDbService messageService;

    public Console(UserDbService userService, FriendshipDbService friendService
            , FriendRequestDbService friendRequestDbService, MessageDbService messageDbService) {
        this.userService = userService;
        this.friendshipService = friendService;
        this.friendRequestService = friendRequestDbService;
        this.messageService = messageDbService;

        runner();
    }

    void addUser() {

        Scanner scan = new Scanner(System.in);

        System.out.println("Add user");
        System.out.print("Enter first name: ");
        String firstName = scan.nextLine();
        System.out.print("Enter last name: ");
        String lastName = scan.nextLine();
        System.out.print("Enter password: ");
        String password = scan.nextLine();

        User new_user = new User(firstName, lastName, password);

        userService.add(new_user);
        System.out.println(new_user + " successfully added");

    }

    void deleteUser() {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter user's id: ");
        String id_str = scan.nextLine();
        User deleted_user = null;

        Long id = Long.parseLong(id_str);
        try {
            deleted_user = userService.delete(id);
            System.out.println("User with UID: " + deleted_user.getId() + " successfully deleted");
        } catch (RepoException repoException) {
            System.out.println(repoException.getMessage());
        }


    }

    void findFriends() {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter user's id: ");
        String id_str = scan.nextLine();

        Iterable<UserDateDto> friendsList = userService.getFriends(Long.parseLong(id_str));
        friendsList.forEach(System.out::println);
    }

    void findFriendsInMonth(){

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter user's id: ");
        String id_str = scan.nextLine();
        System.out.print("Enter the month: ");
        String month = scan.nextLine();

        Iterable<UserDateDto> friendsList = userService.getFriendsInMonth(Long.parseLong(id_str), Integer.parseInt(month));
        friendsList.forEach(System.out::println);

    }

    void addFriend() {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter first user's id: ");
        String user1_id = scan.nextLine();
        System.out.print("Enter second user's id: ");
        String user2_id = scan.nextLine();

        friendshipService.add(new Friendship(Long.parseLong(user1_id), Long.parseLong(user2_id)));
        System.out.println("Friendship added");


    }

    void deleteFriend() {

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter first user's id: ");
        String user1_id = scan.nextLine();
        System.out.print("Enter second user's id: ");
        String user2_id = scan.nextLine();

        Tuple<Long, Long> id = new Tuple<>(Long.parseLong(user1_id), Long.parseLong(user2_id));
        friendshipService.delete(id);
        System.out.println("The friendship with id = " + id + " has been deleted");

    }


    private Long login() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Login user id: ");
        String uid = scan.nextLine();
        System.out.println("Password: ");
        String password = scan.nextLine();

        User user =  userService.login(Long.parseLong(uid), password);
        System.out.println("Logged in as: " + user.getFirstName() + " " + user.getLastName() + "\n");
        return Long.parseLong(uid);

    }

    private void sendFriendRequest() {

        Long login_id = login();
        Scanner scan = new Scanner(System.in);
        System.out.print("Send friend request to: ");
        String request_id = scan.nextLine();

        friendRequestService.sendFriendRequest(login_id, Long.parseLong(request_id));
        System.out.println("Friend request sent");

    }

    private void printUsersFriendRequests(Long id){
        System.out.println("Friend requests:");
        friendRequestService.getUsersFriendRequests(id).forEach(System.out::println);
        System.out.println();
    }

    private void acceptFriendRequest() {

        Long login_id = login();
        printUsersFriendRequests(login_id);

        Scanner scan = new Scanner(System.in);
        System.out.print("Accept friend request from: ");
        String accept_id = scan.nextLine();

        FriendRequest friendRequest = friendRequestService.acceptFriendRequest(login_id, Long.parseLong(accept_id));
        System.out.println("Friend request from user with UID = " + friendRequest.getId().getLeft() + " accepted");

    }

    private void rejectFriendRequest() {

        Long login_id = login();
        printUsersFriendRequests(login_id);

        Scanner scan = new Scanner(System.in);
        System.out.print("Reject friend request from: ");
        String reject_id = scan.nextLine();

        FriendRequest friendRequest = friendRequestService.rejectFriendRequest(login_id, Long.parseLong(reject_id));
        System.out.println("Friend request from user with UID = " + friendRequest.getId().getLeft() + " rejected");

    }

    private void sendMessage() {

        Long login_id = login();
        System.out.println("\nFriends: ");
        Iterable<UserDateDto> friendsList = userService.getFriends(login_id);
        friendsList.forEach(System.out::println);

        Scanner scan = new Scanner(System.in);
        System.out.print("Send message to: ");
        String to_user_str = scan.nextLine();
        System.out.println("Type your message: ");
        String text = scan.nextLine();

        List<Long> to_users = new ArrayList<>();
        Arrays.stream(to_user_str.split(" ")).forEach(x -> to_users.add(Long.parseLong(x)));

        messageService.sendMessage(login_id, to_users, text, false);
        System.out.println("Message sent");

    }

    private void printConversation() {

        Long login_id = login();
        System.out.println("\nFriends: ");
        Iterable<UserDateDto> friendsList = userService.getFriends(login_id);
        friendsList.forEach(System.out::println);

        Scanner scan = new Scanner(System.in);
        System.out.print("Print conversation with: ");
        String user_str = scan.nextLine();

        Iterable<Message> messages = messageService.getConversation(login_id, Long.parseLong(user_str));
        System.out.println("\nConversation with " + user_str + ": ");
        messages.forEach(System.out::println);

    }

    private void replyMessage() {

        Long login_id = login();

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the id of the message to reply to: ");
        String message_id = scan.nextLine();

        System.out.print("Enter who to reply to (all/one): ");
        String reply_to = scan.nextLine();

        System.out.println("Type your message: ");
        String text = scan.nextLine();

        if(reply_to.equals("all"))
            messageService.replyToAll(login_id, Long.parseLong(message_id), text, true);
        else if (reply_to.equals("one")){
            messageService.replyTo(login_id, Long.parseLong(message_id), text);
        }
        else {
            System.out.println("The argument doesn't match neither of the variants");
            return;
        }
        System.out.println("Reply sent");

    }

    public void runner() {

        System.out.println("\nFor the list of commands enter 'menu' command\n");

        System.out.print("Command: ");
        Scanner scan = new Scanner(System.in);
        String command = scan.nextLine();

        while (!command.equals("exit")) {

            switch (command) {
                case "menu":
                    System.out.println("Command list:\n" +
                            "   add user\n" +
                            "   print users\n" +
                            "   delete user\n" +
                            "   add friend\n" +
                            "   print friends\n" +
                            "   delete friend\n" +
                            "   communities\n" +
                            "   most sociable\n" +
                            "   find friends\n" +
                            "   find friends in month\n" +
                            "   send friend request\n" +
                            "   accept friend request\n" +
                            "   reject friend request\n" +
                            "   send message\n" +
                            "   reply message\n" +
                            "   print conversation\n" +
                            "   exit\n");
                    break;

                case "add user":
                    try {
                        addUser();
                    } catch (ValidationException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                case "print users":
                    Iterable<User> l = userService.getAll();
                    l.forEach(System.out::println);
                    break;

                case "delete user":
                    try {
                        deleteUser();
                    } catch (IllegalArgumentException iae) {
                        System.out.println("The id must be a number!");
                    } catch (RepoException repoException) {
                        System.out.println(repoException.getMessage());
                    }
                    break;

                case "find friends":
                    try{

                        findFriends();

                    } catch (IllegalArgumentException illegalArgumentException) {
                        System.out.println("The id must be an integer");
                    }
                    catch (ServiceException serviceException) {
                        System.out.println(serviceException.getMessage());
                    }
                    break;

                case "find friends in month":
                    try{
                        findFriendsInMonth();
                    } catch (IllegalArgumentException illegalArgumentException){
                        System.out.println("User id and month must be integers");
                    } catch (ServiceException serviceException) {
                        System.out.println(serviceException.getMessage());
                    }
                    break;

                case "add friend":
                    try {
                        addFriend();
                    } catch (IllegalArgumentException illegalArgumentException) {
                        System.out.println("Users' ids must be numbers");
                        break;
                    } catch (ValidationException | RepoException | ServiceException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                case "print friends":
                    Iterable<Friendship> friendships = friendshipService.getAll();
                    friendships.forEach(System.out::println);
                    break;

                case "delete friend":
                    try {
                        deleteFriend();
                    } catch (IllegalArgumentException illegalArgumentException) {
                        System.out.println("The ids must be numbers!");
                    } catch (RepoException repoException) {
                        System.out.println(repoException.getMessage());
                    }
                    break;

                case "communities":
                    int communities_count = friendshipService.communitiesCount();
                    System.out.println("The number of communities: " + communities_count);
                    break;

                case "most sociable":
                    Iterable<Long> community = friendshipService.mostSociableCommunity();
                    System.out.print("The most sociable community: ");
                    community.forEach(x -> System.out.print(x + " "));
                    break;

                case "send friend request":
                    try {
                        sendFriendRequest();
                    } catch (IllegalArgumentException illegalArgumentException){
                        System.out.println("Users ids must be integers");
                    } catch (ServiceException | RepoException | ValidationException serviceException){
                        System.out.println(serviceException.getMessage());
                    }
                    break;

                case "accept friend request":
                    try{
                        acceptFriendRequest();
                    } catch (IllegalArgumentException illegalArgumentException){
                        System.out.println("Users ids must be integers");
                    } catch (RepoException | ServiceException repoException) {
                        System.out.println(repoException.getMessage());
                    }
                    break;

                case "reject friend request":
                    try{
                        rejectFriendRequest();
                    } catch (IllegalArgumentException illegalArgumentException){
                        System.out.println("Users ids must be integers");
                    } catch (RepoException | ServiceException repoException) {
                        System.out.println(repoException.getMessage());
                    }
                    break;

                case "send message":
                    try{
                        sendMessage();
                    } catch (IllegalArgumentException illegalArgumentException) {
                        System.out.println("Users ids must be integers");
                    } catch (ServiceException | ValidationException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;


                case "print conversation":
                    try{
                        printConversation();
                    } catch(IllegalArgumentException illegalArgumentException) {
                        System.out.println("Users ids must be integers");
                    }
                    break;

                case "reply message":
                    try{
                        replyMessage();
                    } catch (IllegalArgumentException illegalArgumentException) {
                        System.out.println("Ids must be integers");
                    } catch (ServiceException | ValidationException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                default:
                    System.out.println("Command not found!");
            }

            System.out.println("\nFor the list of commands enter 'menu' command");
            System.out.print("\nCommand: ");
            command = scan.nextLine();

        }

    }

}
