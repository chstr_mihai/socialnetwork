package socialnetwork.statistics;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import socialnetwork.domain.User;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class UserActivityLog {

    PDPage page;
    PDDocument document;
    PDPageContentStream pageContentStream;
    private final User user;
    private final LocalDate from;
    private final LocalDate to;
    private final String fileName;

    public UserActivityLog(User user, LocalDate from, LocalDate to, String fileName) {

        this.user = user;
        this.from = from;
        this.to = to;
        this.fileName = fileName;

        setUp();

    }

    private void setUp() {
        try {
            document = new PDDocument();
            page = new PDPage();
            document.addPage(page);

            pageContentStream = new PDPageContentStream(document, page);
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 22);
            pageContentStream.newLineAtOffset(230, 720);
            pageContentStream.showText("Activity Log");
            pageContentStream.endText();

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, 680);
            pageContentStream.showText("Name: " + user.getFirstName() + " " + user.getLastName());
            pageContentStream.endText();

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, 660);
            pageContentStream.showText("Your activity between: " + from.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " and " + to.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            pageContentStream.endText();


            PDImageXObject imageXObject = PDImageXObject.createFromFile("D:\\facultate\\An2\\MAP\\proiect-lab-schelet\\src\\main\\resources\\images\\NanoPic3.jpg", document);
            imageXObject.setHeight(146);
            imageXObject.setWidth(200);
            pageContentStream.drawImage(imageXObject, 400, 620);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public void createPdf(List<String> documentRows) throws IOException{

        try {

            insertRows(documentRows);
            pageContentStream.close();
            document.save("C:\\Users\\mihai\\Desktop\\" + fileName.replace(" ", "") + ".pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int checkEndOfPage(int y) throws IOException{
        if (y - 40 <= 0) {
            y = 750;
            PDPage newPage = new PDPage();
            document.addPage(newPage);
            try{
                pageContentStream.close();
                pageContentStream = new PDPageContentStream(document, newPage);
            }catch (IOException ioException){
                throw new IOException();
            }
        }
        return y;
    }

    private void insertRows(List<String> documentRows) throws IOException{

        int y = 540;

        for (String row : documentRows) {
            try {
                y = checkEndOfPage(y);

                pageContentStream.beginText();
                if(row.length() != 0 && row.charAt(row.length() - 1) == ':')
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
                else
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                String firstHalfOfRow = row.split("\n")[0];
                pageContentStream.showText(firstHalfOfRow.replace("\r", ""));

                if (row.split("\n").length == 2) {
                    pageContentStream.endText();
                    y -= 20;

                    pageContentStream.beginText();
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                    pageContentStream.newLineAtOffset(50, y);
                    String secondHalfOfRow = row.split("\n")[1];
                    pageContentStream.showText(secondHalfOfRow.replace("\r", ""));
                }

                pageContentStream.endText();

                y -= 20;

            } catch (IOException ioException1) {
                throw new IOException();
            }
        }
    }
}


