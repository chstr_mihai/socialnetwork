package socialnetwork.service;

import socialnetwork.domain.Notification;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.utils.events.Event;
import socialnetwork.utils.events.EventType;
import socialnetwork.utils.events.NotificationEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class NotificationDbService implements Observable<Event> {

    private final Repository<Long, Notification> notificationRepository;

    private List<Observer<Event>> observers = new ArrayList<>();

    public NotificationDbService(Repository<Long, Notification> notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public Notification add(Notification notification) {

        notificationRepository.save(notification);
        notifyObservers(new NotificationEvent(EventType.NOTIFICATION));
        return notification;

    }

    public List<Notification> addMultiple(List<Notification> notifications) {

        notifications.forEach(notificationRepository::save);
        notifyObservers(new NotificationEvent(EventType.NOTIFICATION));
        return notifications;

    }

    public Notification open(Notification notification) {
        return notificationRepository.update(notification);
    }

    public List<Notification> getUsersNotifications(Long uid) {

        Iterable<Notification> notifications = notificationRepository.findAll();
        return StreamSupport.stream(notifications.spliterator(), false)
                .filter(x -> x.getTargetedUser().equals(uid))
                .sorted(Comparator.comparing(Notification::getDate).reversed())
                .peek(x -> {
                    if(!x.isOpened())
                        x.setText("*" + x.getText());
                })
                .collect(Collectors.toList());

    }

    public boolean openUsersNotifications(List<Notification> notifications) {

        //notifications.forEach(notificationRepository::update);
        if(notifications.size() != 0)
            notificationRepository.update(notifications.get(0));
        return true;

    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x->x.update(t));
    }
}
