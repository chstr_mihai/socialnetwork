package socialnetwork.service;

import socialnetwork.domain.Participant;
import socialnetwork.domain.ScheduledEvent;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.events.Event;
import socialnetwork.utils.events.EventType;
import socialnetwork.utils.events.ScheduledEventEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class EventDbService implements Observable<Event> {

    private final PagingRepository<Long, ScheduledEvent> eventRepository;
    private final Repository<Tuple<Long, Long>, Participant> participantRepository;

    private final int pageSizeEvents = 13;
    private int pageEvents = 0;

    private List<Observer<Event>> observers=new ArrayList<>();

    public EventDbService(PagingRepository<Long, ScheduledEvent> eventRepository
            , Repository<Tuple<Long, Long>, Participant> participantRepository) {

        this.eventRepository = eventRepository;
        this.participantRepository = participantRepository;

    }

    public ScheduledEvent addEvent(ScheduledEvent event) {

        ScheduledEvent savedEvent = eventRepository.save(event);
        participantRepository.save(new Participant(event.getId(), event.getJoinedUsers().get(0).getId(), true));
        notifyObservers(new ScheduledEventEvent(EventType.SCHEDULEDEVENT));
        return savedEvent;

    }

    public List<ScheduledEvent> getEvents() {
        return StreamSupport.stream(eventRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public Participant updateJoiningStatus(Tuple<Long, Long> pid) {

        Participant participant = participantRepository.findOne(pid);

        if(participant != null) {
            participantRepository.delete(pid);
            participant = null;
        }
        else {
            participant = new Participant(pid);
            participantRepository.save(participant);
        }

        notifyObservers(new ScheduledEventEvent(EventType.SCHEDULEDEVENT));
        return participant;

    }

    public Participant turnNotificationsOnOff(Participant participant) {

        participant = participantRepository.update(participant);
        notifyObservers(new ScheduledEventEvent(EventType.SCHEDULEDEVENT));
        return participant;

    }

    public List<ScheduledEvent> getEventsWithNotificationsOn(User loggedUser) {

        Map<Tuple<Long, Long>, Boolean> participations = StreamSupport.stream(participantRepository.findAll().spliterator(), false)
                .filter(x -> x.getId().getRight().equals(loggedUser.getId()) && x.isNotificationsOn())
                .collect(Collectors.toMap(Participant::getId, Participant::isNotificationsOn));

        return StreamSupport.stream(eventRepository.findAll().spliterator(), false)
                .filter(x -> x.getJoinedUsers().contains(loggedUser) &&
                        participations.get(new Tuple<>(x.getId(), loggedUser.getId())) != null &&
                        participations.get(new Tuple<>(x.getId(), loggedUser.getId())).equals(true))
                .collect(Collectors.toList());

    }

    public Participant getOne(Tuple<Long, Long> pid) {
        return participantRepository.findOne(pid);
    }

    public List<ScheduledEvent> getNextEventsOnPage() {

        this.pageEvents++;
        List<ScheduledEvent> events = StreamSupport.stream(eventRepository.findAll().spliterator(), false).collect(Collectors.toList());
        if (this.pageSizeEvents >= events.size()) {
            this.pageEvents = 0;
            return getEventsOnPage(0);
        }
        if (this.pageEvents + this.pageSizeEvents > events.size()) {
            this.pageEvents--;
        }
        return getEventsOnPage(pageEvents);

    }

    public List<ScheduledEvent> getPreviousEventsOnPage() {

        this.pageEvents -= 1;

        if(pageEvents < 0)
            this.pageEvents = 0;

        return getEventsOnPage(pageEvents);

    }

    public List<ScheduledEvent> getEventsOnPage(int page) {

        this.pageEvents = page;
        Pageable pageable = new PageableImplementation(page, this.pageSizeEvents);
        Page<ScheduledEvent> eventPage = eventRepository.findAll(pageable);
        return eventPage.getContent().collect(Collectors.toList());

    }

    public int getEventsCurrentPage() {
        return this.pageEvents;
    }


    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x->x.update(t));
    }

}
