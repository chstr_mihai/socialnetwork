package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.utils.events.*;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageDbService implements Observable<Event> {
    private final Repository<Long, Message> messageRepository;
    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;
    private final Validator<Message> validator;
    private final Repository<Long, User> userRepository;

    private List<Observer<Event>> observers=new ArrayList<>();

    public MessageDbService(Repository<Long, Message> messageRepository
            , Validator<Message> validator, Repository<Long, User> userRepository
            , Repository<Tuple<Long, Long>, Friendship> friendshipRepository) {
        this.messageRepository = messageRepository;
        this.friendshipRepository = friendshipRepository;
        this.validator = validator;
        this.userRepository = userRepository;
    }

    public Message sendMessage(Long from_user, List<Long> to_users, String text, boolean isGroup) {

        List<User> to_list = new ArrayList<>();
        User from = userRepository.findOne(from_user);
        StringBuilder userDoesntExistException = new StringBuilder();
        to_users.forEach(x -> {
            User user = userRepository.findOne(x);
            if(user != null) {
                    to_list.add(user);
            }
            else
                userDoesntExistException.append(x).append(" ");

        });

        Message message = new Message(from, to_list, text, isGroup);
        validator.validate(message);

        String exception = "";
        if(userDoesntExistException.length() > 0)
            exception += "The users with ids: " + userDoesntExistException + "don't exist\n";

        if(exception.length() > 0)
            throw new ServiceException(exception);

        messageRepository.save(message);

        notifyObservers(new MessageEvent(EventType.MESSAGE));

        return message;

    }

    public Iterable<Message> getConversation(Long login_id, Long to_user_id) {

        Iterable<Message> all_messages = messageRepository.findAll();
        List<Message> conversation = new ArrayList<>();
        User to_user = userRepository.findOne(to_user_id), from_user = userRepository.findOne(login_id);
        all_messages.forEach(conversation::add);

        return conversation
                .stream()
                .filter(x -> (x.getFrom_user().equals(from_user) && x.getTo_user().contains(to_user))
                        || (x.getFrom_user()).equals(to_user) && x.getTo_user().contains(from_user))
                .filter(x -> !x.isGroupMessage())
                .sorted(Comparator.comparing(Message::getDate))
                .collect(Collectors.toList());

    }

    public Iterable<List<User>> getGroups(Long login_id) {

        Iterable<Message> allMessages = messageRepository.findAll();
        List<List<User>> groupChats = new ArrayList<>();
        User logged_user = userRepository.findOne(login_id);

        allMessages.forEach(x -> {
            if(x.getTo_user().size() > 1) {
                List<User> tos = x.getTo_user();
                tos.add(x.getFrom_user());
                if (tos.contains(logged_user)) {
                    tos.remove(logged_user);
                    groupChats.add(tos);
                }
            }
        });

         List<List<User>> list = new ArrayList<>();
         groupChats.forEach(l -> list.add(l.stream().sorted().collect(Collectors.toList())));
         return list.stream().distinct().collect(Collectors.toList());

    }

    public Iterable<Message> getGroupChat(User loggedUser, List<User> members){
        Iterable<Message> allMessages = messageRepository.findAll();
        List<User> membersCopy = new ArrayList<>(members);
        membersCopy.add(loggedUser);

        return StreamSupport.stream(allMessages.spliterator(), false)
                .filter(message -> {
                    List<User> to_users = message.getTo_user();
                    to_users.add(message.getFrom_user());
                    if(membersCopy.size() != to_users.size())
                        return false;
                    return to_users.containsAll(membersCopy);
                })
                .collect(Collectors.toList());

    }

    public Message replyToAll(Long login_id, Long message_id, String text, boolean isGroup) {

        Message message = messageRepository.findOne(message_id);
        if(message == null)
            throw new ServiceException("The message you want to reply to doesn't exist");
        User from_user = userRepository.findOne(login_id);
        List<User> to_list = new ArrayList<>();
        message.getTo_user().forEach(x -> {
            if(!x.equals(from_user))
                to_list.add(x);
        });
        to_list.add(message.getFrom_user());

        ReplyMessage replyMessage = new ReplyMessage(from_user, to_list, text, message, isGroup);
        validator.validate(replyMessage);
        messageRepository.save(replyMessage);

        notifyObservers(new MessageEvent(EventType.MESSAGE));

        return replyMessage;

    }

    public Message replyTo(Long login_id, Long message_id, String text) {

        Message message = messageRepository.findOne(message_id);
        if(message == null)
            throw new ServiceException("The message you want to reply to doesn't exist");

        User from_user = userRepository.findOne(login_id);
        List<User> to_user = new ArrayList<>();
        to_user.add(message.getFrom_user());
        ReplyMessage replyMessage = new ReplyMessage(from_user, to_user, text, message, false);
        validator.validate(replyMessage);
        messageRepository.save(replyMessage);

        notifyObservers(new MessageEvent(EventType.MESSAGE));

        return replyMessage;

    }

    public List<Message> getMessagesBetweenDates(User loggedUser, LocalDate fromDate, LocalDate toDate) {

        LocalDateTime fromDateTime = fromDate.atStartOfDay();
        LocalDateTime toDateTime = toDate.atStartOfDay();

        Iterable<Message> messages = messageRepository.findAll();

        return StreamSupport.stream(messages.spliterator(), false)
                .filter(x -> x.getTo_user().contains(loggedUser))
                .filter(x -> x.getDate().isAfter(fromDateTime) && x.getDate().isBefore(toDateTime))
                .collect(Collectors.toList());

    }


    public List<Message> getMessagesFromFriendBetweenDates(User loggedInUser, User friend
            , LocalDate logDateFrom, LocalDate logDateTo) {

        return getMessagesBetweenDates(loggedInUser, logDateFrom, logDateTo)
                .stream()
                .filter(x -> x.getFrom_user().equals(friend))
                .collect(Collectors.toList());

    }

    public Message getOne(Long id) {
        return messageRepository.findOne(id);
    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x->x.update(t));
    }

}
