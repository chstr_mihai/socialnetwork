package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.utils.events.Event;
import socialnetwork.utils.events.EventType;
import socialnetwork.utils.events.FriendRequestEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendRequestDbService implements Observable<Event> {
    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;
    private final Validator<FriendRequest> validator;
    private final Repository<Long, User> userRepository;
    private final Repository<Tuple<Long, Long>, FriendRequest> friendRequestRepository;

    int pageSentFriendRequests = 0;
    int sizeSentFriendRequests = 10;

    int pageSizeReceivedFriendRequests = 10;
    int pageReceivedFriendRequests = 0;

    private List<Observer<Event>> observers = new ArrayList<>();

    public FriendRequestDbService(Repository<Tuple<Long, Long>, Friendship> friendshipRepository
            , Validator<FriendRequest> validator, Repository<Long, User> userRepository
            , Repository<Tuple<Long, Long>, FriendRequest> friendRequestRepository) {
        this.friendshipRepository = friendshipRepository;
        this.validator = validator;
        this.userRepository = userRepository;
        this.friendRequestRepository = friendRequestRepository;
    }

    public void sendFriendRequest(Long login_id, Long request_id) {
        FriendRequest friendRequest = new FriendRequest(login_id, request_id);
        validator.validate(friendRequest);
        User user = userRepository.findOne(request_id);
        if (user == null)
            throw new ServiceException("The user with id = " + request_id + " doesn't exist");
        Friendship friendship = friendshipRepository.findOne(new Tuple<>(login_id, request_id));
        if (friendship != null)
            throw new ServiceException("You are already friends with this user");

        friendRequestRepository.save(friendRequest);
        notifyObservers(new FriendRequestEvent(EventType.FRIENDREQUEST));

    }

    public Iterable<FriendRequest> getUsersFriendRequests(Long id) {

        Iterable<FriendRequest> allFriendRequests = friendRequestRepository.findAll();
        List<FriendRequest> friendRequests = new ArrayList<>();
        allFriendRequests.forEach(friendRequests::add);
        List<FriendRequest> usersFriendRequests = friendRequests
                .stream()
                .filter(x -> x.getId().getRight().equals(id))
                .sorted(Comparator.comparing(FriendRequest::getDate).reversed())
                .collect(Collectors.toList());
        if (usersFriendRequests.size() == 0)
            throw new ServiceException("There are no friend requests left");

        return usersFriendRequests;

    }

    public Iterable<UserFriendRequestDto> getUsersFriendRequestsDto(Long id) {

        Iterable<FriendRequest> allFriendRequests = friendRequestRepository.findAll();
        List<FriendRequest> friendRequests = new ArrayList<>();
        allFriendRequests.forEach(friendRequests::add);
        List<FriendRequest> usersFriendRequests = friendRequests
                .stream()
                .filter(x -> x.getId().getRight().equals(id))
                .collect(Collectors.toList());

        return usersFriendRequests.stream()
                .map(x -> {
                    User user = userRepository.findOne(x.getId().getLeft());
                    return new UserFriendRequestDto(x.getId().getLeft(), x.getId().getRight(), user.getFirstName(), user.getLastName(), x.getStatus(), x.getDate());
                })
                .sorted(Comparator.comparing(UserFriendRequestDto::getDate).reversed())
                .collect(Collectors.toList());

    }

    public FriendRequest acceptFriendRequest(Long login_id, Long accept_id) {
        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<>(accept_id, login_id));
        if (friendRequest == null || !friendRequest.getStatus().equals("pending"))
            throw new ServiceException("The friend request doesn't exist");

        friendRequest.setStatus("accepted");
        FriendRequest acceptedFriendRequest = friendRequestRepository.update(friendRequest);
        friendshipRepository.save(new Friendship(friendRequest.getId().getLeft(), friendRequest.getId().getRight()));
        notifyObservers(new FriendRequestEvent(EventType.FRIENDREQUEST));

        return acceptedFriendRequest;

    }

    public FriendRequest rejectFriendRequest(Long login_id, long reject_id) {

        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<>(reject_id, login_id));
        if (friendRequest == null || !friendRequest.getStatus().equals("pending"))
            throw new ServiceException("The friend request doesn't exist");

        friendRequest.setStatus("rejected");
        friendRequest = friendRequestRepository.update(friendRequest);
        notifyObservers(new FriendRequestEvent(EventType.FRIENDREQUEST));

        return friendRequest;

    }

    public FriendRequest getOne(Long loggedUser, Long otherUser) {
        return friendRequestRepository.findOne(new Tuple<>(loggedUser, otherUser));
    }

    public FriendRequest cancelFriendRequest(Long loggedUser, Long receiver) {

        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<>(loggedUser, receiver));
        if (friendRequest == null || !friendRequest.getStatus().equals("pending"))
            throw new ServiceException("The friend request doesn't exist");
        friendRequest.setStatus("cancelled");
        friendRequest = friendRequestRepository.update(friendRequest);
        notifyObservers(new FriendRequestEvent(EventType.FRIENDREQUEST));

        return friendRequest;

    }

    public Iterable<UserFriendRequestDto> getUsersSentFriendRequestsDto(Long id) {

        Iterable<FriendRequest> allFriendRequests = friendRequestRepository.findAll();
        List<FriendRequest> friendRequests = new ArrayList<>();
        allFriendRequests.forEach(friendRequests::add);
        List<FriendRequest> usersFriendRequests = friendRequests
                .stream()
                .filter(x -> x.getId().getLeft().equals(id))
                .collect(Collectors.toList());

        return usersFriendRequests.stream()
                .map(x -> {
                    User user = userRepository.findOne(x.getId().getRight());
                    return new UserFriendRequestDto(x.getId().getLeft(), x.getId().getRight(), user.getFirstName(), user.getLastName(), x.getStatus(), x.getDate());
                })
                .sorted(Comparator.comparing(UserFriendRequestDto::getDate).reversed())
                .collect(Collectors.toList());

    }



    public List<UserFriendRequestDto> getNextReceivedFriendRequests(Long id) {
        this.pageReceivedFriendRequests++;
        List<UserFriendRequestDto> friendRequests = StreamSupport.stream(getUsersFriendRequestsDto(id).spliterator()
                ,false).collect(Collectors.toList());
        if(this.pageSizeReceivedFriendRequests >= friendRequests.size()){
            this.pageReceivedFriendRequests = 0;
            return getReceivedFriendRequestsOnPage(0,id);
        }
        if (this.pageReceivedFriendRequests + this.pageSizeReceivedFriendRequests > friendRequests.size()) {
            this.pageReceivedFriendRequests--;
        }
        Pageable pageable = new PageableImplementation(pageReceivedFriendRequests, this.pageSizeReceivedFriendRequests);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, friendRequests);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserFriendRequestDto> getPreviousReceivedFriendRequests(Long id) {
        this.pageReceivedFriendRequests--;
        if (this.pageReceivedFriendRequests <= 0) {
            this.pageReceivedFriendRequests = 0;
        }
        Pageable pageable = new PageableImplementation(pageReceivedFriendRequests, this.pageSizeReceivedFriendRequests);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, getUsersFriendRequestsDto(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserFriendRequestDto> getReceivedFriendRequestsOnPage(int page, Long id) {
        this.pageReceivedFriendRequests = page;
        Pageable pageable = new PageableImplementation(page, this.pageSizeReceivedFriendRequests);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, getUsersFriendRequestsDto(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public int getReceivedFriendRequestsCurrentPage() {
        return this.pageReceivedFriendRequests;
    }


    public List<UserFriendRequestDto> getNextSentFriendRequests(Long id) {
        this.pageSentFriendRequests++;
        List<UserFriendRequestDto> friendRequests = StreamSupport.stream(getUsersSentFriendRequestsDto(id).spliterator()
                ,false).collect(Collectors.toList());
        if(this.sizeSentFriendRequests >= friendRequests.size()){
            this.pageSentFriendRequests = 0;
            return getSentFriendRequestsOnPage(0,id);
        }
        if (this.pageSentFriendRequests + this.sizeSentFriendRequests > friendRequests.size()) {
            this.pageSentFriendRequests--;
        }
        Pageable pageable = new PageableImplementation(pageSentFriendRequests, this.sizeSentFriendRequests);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, friendRequests);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserFriendRequestDto> getPreviousSentFriendRequests(Long id) {
        this.pageSentFriendRequests--;
        if (this.pageSentFriendRequests <= 0) {
            this.pageSentFriendRequests = 0;
        }
        Pageable pageable = new PageableImplementation(pageSentFriendRequests, this.sizeSentFriendRequests);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, getUsersSentFriendRequestsDto(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserFriendRequestDto> getSentFriendRequestsOnPage(int page, Long id) {
        this.pageSentFriendRequests = page;
        Pageable pageable = new PageableImplementation(page, this.sizeSentFriendRequests);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, getUsersSentFriendRequestsDto(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public int getSentFriendRequestsCurrentPage() {
        return this.pageSentFriendRequests;
    }


    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x->x.update(t));
    }

}
