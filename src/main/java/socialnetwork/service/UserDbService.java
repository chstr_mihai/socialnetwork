package socialnetwork.service;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.UserDateDto;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.Paginator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UserDbService {
    private final Repository<Long, User> userRepository;
    private final Validator<User> validator;
    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;

    private final int pageSizeFindFriends = 14;
    private int pageFindFriends = 0;
    private int pageFilteredFindFriends = 0;

    private final int pageSizeFriends = 9;
    private int pageFriends = 0;
    private int pageFilteredFriends = 0;

    public UserDbService(Repository<Long, User> userDbRepository, Validator<User> validator
            , Repository<Tuple<Long, Long>, Friendship> friendshipRepository) {

        this.userRepository = userDbRepository;
        this.validator = validator;
        this.friendshipRepository = friendshipRepository;

    }

    public String hashPassword(String password) {

        String hashedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            hashedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return hashedPassword;

    }

    public User add(User user) {

        validator.validate(user);
        user.setChildhoodNickname(hashPassword(user.getChildhoodNickname()));
        user.setPassword(hashPassword(user.getPassword()));
        return userRepository.save(user);

    }

    public User delete(Long id) {
        return userRepository.delete(id);
    }

    public List<UserDateDto> getFriendsInMonth(Long uid, int month) {

        if(month < 1 || month > 12)
            throw new ServiceException("The month value must be between 1 and 12");

        return getFriends(uid)
                .stream()
                .filter(x -> x.getDate().getMonthValue() == month)
                .collect(Collectors.toList());

    }

    public List<UserDateDto> getFriendshipsBetweenDates(User loggedUser, LocalDate fromDate, LocalDate toDate) {

        LocalDateTime fromDateTime = fromDate.atStartOfDay();
        LocalDateTime toDateTime = toDate.atStartOfDay();

        return getFriends(loggedUser.getId())
                .stream()
                .filter(x -> x.getDate().isAfter(fromDateTime) && x.getDate().isBefore(toDateTime))
                .collect(Collectors.toList());

    }


    public List<UserDateDto> getFriends(Long uid) {

        User user = userRepository.findOne(uid);
        if(user == null)
            throw new ServiceException("The user with id = " + uid + " doesn't exist");

        return user.getFriends()
                .stream()
                .map(x -> new UserDateDto(x.getId(), x.getFirstName(), x.getLastName()
                        , friendshipRepository.findOne(new Tuple<>(uid, x.getId())).getDate()))
                .collect(Collectors.toList());

    }


    public List<User> getUsersNonFriends(Long uid) {
        User user = userRepository.findOne(uid);
        if(user == null)
            throw new ServiceException("The user with id = " + uid + " doesn't exist");
        List<User> friendsList = user.getFriends();

        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .filter(x -> !friendsList.contains(x) && !x.getId().equals(uid))
                .collect(Collectors.toList());

    }

    public User login(Long uid, String password) {
        User user = userRepository.findOne(uid);
        password = hashPassword(password);
        if(user == null || !user.getPassword().equals(password))
            throw new ServiceException("The user with id = " + uid + " doesn't exist");

        return user;
    }

    public User getUser(Long uid) {
        return userRepository.findOne(uid);
    }

    public User resetPassword(Long uid, String nickname, String newPassword) {
        User found = userRepository.findOne(uid);
        if(!found.getChildhoodNickname().equals(hashPassword(nickname)))
            throw new ServiceException("Invalid nickname");
        found.setPassword(hashPassword(newPassword));
        return userRepository.update(found);
    }

    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    public List<User> getFindFriendsOnPage(int page, Long id) {
        this.pageFindFriends = page;
        List<User> nonFriends = getUsersNonFriends(id);
/*
        if(this.pageSizeFindFriends >= nonFriends.size()){
            this.pageFindFriends = 0;
        }
        else if (this.pageFindFriends + pageSizeFindFriends > nonFriends.size()) {
            this.pageFindFriends--;
        }
*/
        Pageable pageable = new PageableImplementation(pageFindFriends, this.pageSizeFindFriends);
        Paginator<User> paginator = new Paginator<>(pageable, nonFriends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<User> getNextFindFriends(Long id) {
        this.pageFindFriends++;
        List<User> friends = getUsersNonFriends(id);
        if(this.pageSizeFindFriends >= friends.size()){
            this.pageFindFriends = 0;
            return getFindFriendsOnPage(0,id);
        }
        if (this.pageFindFriends + pageSizeFindFriends > friends.size()) {
            this.pageFindFriends--;
        }
        Pageable pageable = new PageableImplementation(pageFindFriends, this.pageSizeFindFriends);
        Paginator<User> paginator = new Paginator<>(pageable, friends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<User> getPreviousFindFriends(Long id) {
        this.pageFindFriends--;
        if (this.pageFindFriends <= 0) {
            this.pageFindFriends = 0;
        }
        Pageable pageable = new PageableImplementation(pageFindFriends, this.pageSizeFindFriends);
        Paginator<User> paginator = new Paginator<>(pageable, getUsersNonFriends(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public int getFindFriendsCurrentPage() {
        return this.pageFindFriends;
    }


    public List<UserDateDto> getNextFriends(Long id) {
        this.pageFriends++;
        List<UserDateDto> friends = getFriends(id);
        if (this.pageSizeFriends >= friends.size()) {
            this.pageFriends = 0;
            return getFriendsOnPage(0, id);
        }
        if (this.pageFriends + this.pageSizeFriends > friends.size()) {
            this.pageFriends--;
        }
        Pageable pageable = new PageableImplementation(pageFriends , this.pageSizeFriends);
        Paginator<UserDateDto> paginator = new Paginator<>(pageable, friends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserDateDto> getPreviousFriends(Long id) {
        this.pageFriends--;
        if (this.pageFriends <= 0) {
            this.pageFriends = 0;
        }
        Pageable pageable = new PageableImplementation(pageFriends, this.pageSizeFriends);
        Paginator<UserDateDto> paginator = new Paginator<>(pageable, getFriends(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserDateDto> getFriendsOnPage(int page, Long id) {
        this.pageFriends = page;
        Pageable pageable = new PageableImplementation(page, this.pageSizeFriends);
        Paginator<UserDateDto> paginator = new Paginator<>(pageable, getFriends(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public int getFriendsCurrentPage() {return this.pageFriends;}


    public List<User> getFilteredFindFriendsOnPage(int page, Long id, String cont) {
        this.pageFilteredFindFriends = page;
        List<User> nonFriends = getUsersNonFriends(id)
                .stream()
                .filter(x -> x.toString().toLowerCase().contains(cont.toLowerCase()))
                .collect(Collectors.toList());
        Pageable pageable = new PageableImplementation(pageFilteredFindFriends, this.pageSizeFindFriends);
        Paginator<User> paginator = new Paginator<>(pageable, nonFriends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<User> getNextFilteredFindFriends(Long id, String cont) {
        this.pageFilteredFindFriends++;
        List<User> nonFriends = getUsersNonFriends(id)
                .stream()
                .filter(x -> x.toString().toLowerCase().contains(cont.toLowerCase()))
                .collect(Collectors.toList());
        if(this.pageSizeFindFriends >= nonFriends.size()){
            this.pageFilteredFindFriends = 0;
            return getFilteredFindFriendsOnPage(0,id, cont);
        }
        if (this.pageFilteredFindFriends + pageSizeFindFriends > nonFriends.size()) {
            this.pageFilteredFindFriends--;
        }
        Pageable pageable = new PageableImplementation(pageFilteredFindFriends, this.pageSizeFindFriends);
        Paginator<User> paginator = new Paginator<>(pageable, nonFriends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<User> getPreviousFilteredFindFriends(Long id, String cont) {
        this.pageFilteredFindFriends--;
        if (this.pageFilteredFindFriends <= 0) {
            this.pageFilteredFindFriends = 0;
        }
        List<User> nonFriends = getUsersNonFriends(id)
                .stream()
                .filter(x -> x.toString().toLowerCase().contains(cont.toLowerCase()))
                .collect(Collectors.toList());
        Pageable pageable = new PageableImplementation(pageFilteredFindFriends, this.pageSizeFindFriends);
        Paginator<User> paginator = new Paginator<>(pageable, nonFriends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public int getFilteredFindFriendsCurrentPage() {
        return this.pageFilteredFindFriends;
    }


    public List<UserDateDto> getFilteredFriendsOnPage(int page, Long id, String cont) {
        this.pageFilteredFriends = page;
        List<UserDateDto> friends = getFriends(id)
                .stream()
                .filter(x -> x.toString().toLowerCase().contains(cont.toLowerCase()))
                .collect(Collectors.toList());
        Pageable pageable = new PageableImplementation(pageFilteredFriends, this.pageSizeFriends);
        Paginator<UserDateDto> paginator = new Paginator<>(pageable, friends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserDateDto> getNextFilteredFriends(Long id, String cont) {
        this.pageFilteredFriends++;
        List<UserDateDto> friends = getFriends(id)
                .stream()
                .filter(x -> x.toString().toLowerCase().contains(cont.toLowerCase()))
                .collect(Collectors.toList());
        if(this.pageSizeFriends >= friends.size()){
            this.pageFilteredFriends = 0;
            return getFilteredFriendsOnPage(0,id, cont);
        }
        if (this.pageFilteredFriends + pageSizeFriends > friends.size()) {
            this.pageFilteredFriends--;
        }
        Pageable pageable = new PageableImplementation(pageFilteredFriends, this.pageSizeFriends);
        Paginator<UserDateDto> paginator = new Paginator<>(pageable, friends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<UserDateDto> getPreviousFilteredFriends(Long id, String cont) {
        this.pageFilteredFriends--;
        if (this.pageFilteredFriends <= 0) {
            this.pageFilteredFriends = 0;
        }
        List<UserDateDto> friends = getFriends(id)
                .stream()
                .filter(x -> x.toString().toLowerCase().contains(cont.toLowerCase()))
                .collect(Collectors.toList());
        Pageable pageable = new PageableImplementation(pageFilteredFriends, this.pageSizeFriends);
        Paginator<UserDateDto> paginator = new Paginator<>(pageable, friends);
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public int getFilteredFriendsCurrentPage() {
        return this.pageFilteredFriends;
    }

}
