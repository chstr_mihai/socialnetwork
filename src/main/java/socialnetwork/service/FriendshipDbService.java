package socialnetwork.service;

import org.xml.sax.SAXNotRecognizedException;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Graph;
import socialnetwork.utils.events.*;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendshipDbService implements Observable<Event> {

    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;
    private final Validator<Friendship> validator;
    private final Repository<Long, User> userRepository;

    private List<Observer<Event>> observers=new ArrayList<>();

    public FriendshipDbService(Repository<Tuple<Long, Long>, Friendship> friendshipDbRepository, Validator<Friendship> validator
            , Repository<Long, User> userRepository) {

        this.friendshipRepository = friendshipDbRepository;
        this.validator = validator;
        this.userRepository = userRepository;

    }

    public void add(Friendship friendship) {

        User user1 = userRepository.findOne(friendship.getId().getLeft())
                , user2 = userRepository.findOne(friendship.getId().getRight());
        String exception = "";
        if (user1 == null)
            exception += friendship.getId().getLeft() + " ";
        if(user2 == null)
            exception += friendship.getId().getRight() + " ";
        if(exception.length() > 0)
            throw new ServiceException("The users with ids: " + exception + "don't exist");

        validator.validate(friendship);
        friendshipRepository.save(friendship);
        notifyObservers(new FriendshipEvent(EventType.FRIENDSHIP));

    }

    public Friendship delete(Tuple<Long, Long> id) {

        Friendship friendship = friendshipRepository.delete(id);
        notifyObservers(new FriendshipEvent(EventType.FRIENDSHIP));

        return friendship;

    }

    public Iterable<Friendship> getAll() {
        return friendshipRepository.findAll();
    }

    private Graph createGraph() {

        Iterable<User> users = userRepository.findAll();

        Iterable<Friendship> friendships = friendshipRepository.findAll();
        List<Tuple<Long, Long>> ids = new ArrayList<>();
        friendships.forEach(x -> ids.add(x.getId()));

        Map<Long, Integer> usersIds = new HashMap<>();
        users.forEach(x -> usersIds.put(x.getId(), usersIds.size()));

        return new Graph(userRepository.size(), usersIds, ids);

    }

    public int communitiesCount() {
        Graph network = createGraph();
        return network.getConnectedComponentsCount();
    }

    public Iterable<Long> mostSociableCommunity() {
        Graph network = createGraph();
        return network.getLongestComponent();
    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x->x.update(t));
    }
}
