package socialnetwork.repository.database;

import socialnetwork.domain.ScheduledEvent;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class EventDbRepository implements PagingRepository<Long, ScheduledEvent> {

    private final String url;
    private final String username;
    private final String password;
    Connection connection;
    Long currentId;

    public EventDbRepository(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();
        initIdValue();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private void initIdValue() {

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT MAX(eid) AS max_id FROM events");
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                currentId = resultSet.getLong("max_id");
            else
                currentId = (long)0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public ScheduledEvent findOne(Long eid) {

        ScheduledEvent event = null;

        return null;

    }

    private Map<Long, User> getUsers() {

        Map<Long, User> users = new HashMap<>();
        try{

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Long uid = resultSet.getLong("uid");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");

                User user = new User(first_name, last_name, uid);
                users.put(uid, user);

            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return users;

    }

    private List<User> getJoinedUsers(Long eid, Map<Long, User> users) {

        List<User> joinedUsers = new ArrayList<>();

        try{

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM participants WHERE eid = " + eid);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Long uid = resultSet.getLong("uid");
                joinedUsers.add(users.get(uid));

            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return joinedUsers;

    }

    private List<ScheduledEvent> getEvents() {

        List<ScheduledEvent> events = new ArrayList<>();
        try{

            Map<Long, User> users = getUsers();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM events");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Long eid = resultSet.getLong("eid");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String location = resultSet.getString("location");
                LocalDateTime dateTime = resultSet.getTimestamp("datetime").toLocalDateTime();

                events.add(new ScheduledEvent(eid, name, description, location, dateTime, getJoinedUsers(eid, users)));

            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return events;

    }

    @Override
    public Iterable<ScheduledEvent> findAll() {

        return getEvents();

    }

    @Override
    public ScheduledEvent save(ScheduledEvent event) {

        currentId += 1;
        event.setId(currentId);
        try{

            PreparedStatement statement = connection.prepareStatement("INSERT INTO events VALUES(" +
                    event.getId() + ", '" + event.getName() + "', '" + event.getDescription() + "', '"
                    + event.getLocation() + "', '" + event.getDatetime() + "')");
            statement.execute();

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return event;

    }

    @Override
    public ScheduledEvent delete(Long eid) {
        return null;
    }


    @Override
    public ScheduledEvent update(ScheduledEvent event) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Page<ScheduledEvent> findAll(Pageable pageable) {
        Paginator<ScheduledEvent> paginator = new Paginator<>(pageable, StreamSupport.stream(this.findAll().spliterator(), false)
                .sorted(Comparator.comparing(ScheduledEvent::getDatetime)).collect(Collectors.toList()));
        return paginator.paginate();
    }

}
