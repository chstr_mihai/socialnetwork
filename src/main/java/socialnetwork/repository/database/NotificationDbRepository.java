package socialnetwork.repository.database;

import socialnetwork.domain.Notification;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class NotificationDbRepository implements Repository<Long, Notification> {

    private final String url;
    private final String username;
    private final String password;
    Connection connection;
    Long currentId;

    public NotificationDbRepository(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();
        initIdValue();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private void initIdValue() {

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT MAX(nid) AS max_id FROM notifications");
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                currentId = resultSet.getLong("max_id");
            else
                currentId = (long)0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Notification findOne(Long nid) {

        Notification notification = null;

        try{
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM notifications WHERE nid = " + nid);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next())
                return null;
            Long id = resultSet.getLong("nid");
            Long uid = resultSet.getLong("uid");
            String text = resultSet.getString("text");
            String type = resultSet.getString("type");
            boolean isOpened = resultSet.getBoolean("is_opened");
            LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

            notification = new Notification(id, uid, text, type, isOpened, date);

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return notification;
    }

    @Override
    public Iterable<Notification> findAll() {

        List<Notification> notifications = new ArrayList<>();

        try{
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM notifications");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                Long id = resultSet.getLong("nid");
                Long uid = resultSet.getLong("uid");
                String text = resultSet.getString("text");
                String type = resultSet.getString("type");
                boolean isOpened = resultSet.getBoolean("is_opened");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                notifications.add(new Notification(id, uid, text, type, isOpened, date));

            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }


        return notifications;
    }

    @Override
    public Notification save(Notification notification) {

        currentId += 1;
        notification.setId(currentId);
        try{
            PreparedStatement statement = connection.prepareStatement("INSERT INTO notifications values("
                    + notification.getId() + ", " + notification.getTargetedUser() + ", '" + notification.getText() + "', '"
                    + notification.getType() + "', '" + notification.isOpened() + "', '" + notification.getDate() + "')");
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return notification;
    }

    @Override
    public Notification delete(Long nid) {
        return null;
    }

    @Override
    public Notification update(Notification notification) {

        try{

            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE notifications SET is_opened = true WHERE uid = " + notification.getTargetedUser());
            statement.execute();
            notification.setOpened(true);

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return notification;
    }

    @Override
    public int size() {
        return 0;
    }
}
