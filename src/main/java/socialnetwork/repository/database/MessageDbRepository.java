package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.ReplyMessage;
import socialnetwork.domain.User;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDbRepository implements Repository<Long, Message> {
    private final String url;
    private final String username;
    private final String password;
    Connection connection;
    Long currentId;

    public MessageDbRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();
        initIdValue();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void initIdValue() {

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT MAX(mid) AS max_id FROM messages");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            currentId = resultSet.getLong("max_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Message findOne(Long mid) {

        Message message = null;

        try{

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages WHERE mid = " + mid);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;

            List<User> to_users = findToUsers(resultSet.getLong("mid"));
            boolean isGroup = resultSet.getBoolean("group");
            message = createMessage(resultSet, to_users, isGroup);


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return message;
    }

    private List<User> findToUsers(Long mid) {

        List<User> to_users = new ArrayList<>();
        try{
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users_messages WHERE mid = "
                    + mid);
            ResultSet resultSet1 = statement.executeQuery();
            while (resultSet1.next()) {
                statement = connection.prepareStatement("SELECT * FROM users WHERE uid = "
                        + resultSet1.getLong("uid"));
                ResultSet resultSet2 = statement.executeQuery();
                resultSet2.next();
                to_users.add(new User(resultSet2.getString("first_name"), resultSet2.getString("last_name"),
                        resultSet2.getLong("uid")));
            }

        } catch (SQLException e) {
                System.out.println("Failed to execute query");
        }

        return to_users;

    }

    private Message createMessage(ResultSet resultSet, List<User> to_users, boolean isGroup) {
        Message message = null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE uid = " + resultSet.getLong("from_user"));
            ResultSet resultSet1 = statement.executeQuery();
            resultSet1.next();
            User from_user = new User(resultSet1.getString("first_name"), resultSet1.getString("last_name"),
                    resultSet1.getLong("uid"));

            message = new Message(resultSet.getLong("mid"), from_user,
                    to_users, resultSet.getString("message")
                    , resultSet.getTimestamp("date").toLocalDateTime(), isGroup);

            Long repliedMessage = resultSet.getLong("repliedMessage");
            if(repliedMessage != null)
                message = new ReplyMessage(message, findOne(repliedMessage));

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return message;
    }

    @Override
    public Iterable<Message> findAll() {

        List<Message> messages = new ArrayList<>();
        try{

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                List<User> to_users = findToUsers(resultSet.getLong("mid"));
                boolean isGroup = resultSet.getBoolean("group");
                messages.add(createMessage(resultSet, to_users, isGroup));
            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return messages;
    }

    @Override
    public Message save(Message message) {

        currentId += 1;
        message.setId(currentId);
        try{
            Long repliedMessage = null;
            if(message.getRepliedMessage() != null)
                repliedMessage = message.getRepliedMessage().getId();

            PreparedStatement statement = connection.prepareStatement("INSERT INTO messages VALUES("
                    + currentId + ", " + message.getFrom_user().getId() + ", '" + message.getText()
                    + "', '" + message.getDate() + "', " + repliedMessage + ", '" + message.isGroupMessage() + "')");
            statement.execute();

            message.getTo_user().forEach(x -> {
                try {
                    connection.prepareStatement("INSERT INTO users_messages VALUES(" + x.getId() + ", " + currentId
                            + ")").execute();
                } catch (SQLException e) {
                    System.out.println("Failed to execute query");
                }

            });

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return message;
    }

    @Override
    public Message delete(Long aLong) {
        return null;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }
}
