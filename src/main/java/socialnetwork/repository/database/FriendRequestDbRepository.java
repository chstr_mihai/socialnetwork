package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Tuple;
import socialnetwork.exceptions.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FriendRequestDbRepository implements PagingRepository<Tuple<Long, Long>, FriendRequest> {
    private final String url;
    private final String username;
    private final String password;
    Connection connection;

    public FriendRequestDbRepository(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public FriendRequest findOne(Tuple<Long, Long> fid) {
        FriendRequest friendRequest = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * from friend_requests where from_user = " + fid.getLeft() + "and to_user = " + fid.getRight());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Long from_user = resultSet.getLong("from_user");
                Long to_user = resultSet.getLong("to_user");
                String status = resultSet.getString("status");
                LocalDateTime date = resultSet.getTimestamp("fr_date").toLocalDateTime();

                friendRequest = new FriendRequest(from_user, to_user, status, date);
            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return friendRequest;
    }

    @Override
    public Iterable<FriendRequest> findAll() {
        List<FriendRequest> friendRequests = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * from friend_requests");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long from_user = resultSet.getLong("from_user");
                Long to_user = resultSet.getLong("to_user");
                String status = resultSet.getString("status");
                LocalDateTime date = resultSet.getTimestamp("fr_date").toLocalDateTime();

                FriendRequest friendRequest = new FriendRequest(from_user, to_user, status, date);
                friendRequests.add(friendRequest);
            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }
        return friendRequests;
    }

    @Override
    public FriendRequest save(FriendRequest friendRequest) {

        Long from_user = friendRequest.getId().getLeft(), to_user = friendRequest.getId().getRight();
        FriendRequest foundFriendRequest = findOne(friendRequest.getId());
        if (foundFriendRequest != null) {
            if (foundFriendRequest.getStatus().equals("pending"))
                throw new RepoException("You already sent a friend request to this user");
        } else {
            foundFriendRequest = findOne(new Tuple<>(to_user, from_user));
            if (foundFriendRequest != null) {
                if (foundFriendRequest.getStatus().equals("pending"))
                    throw new RepoException("This user already sent you a friend request");
            }
        }

        try {
            PreparedStatement statement;
            if (foundFriendRequest != null && from_user.equals(foundFriendRequest.getId().getLeft())) {
                statement = connection.prepareStatement("UPDATE friend_requests SET status = 'pending'"
                        + ", fr_date = '" + friendRequest.getDate()
                        + "' WHERE from_user = " + foundFriendRequest.getId().getLeft()
                        + " AND to_user = " + foundFriendRequest.getId().getRight());
            } else {
                statement = connection.prepareStatement("INSERT INTO friend_requests (from_user, to_user, fr_date) values("
                        + friendRequest.getId().getLeft() + ", " + friendRequest.getId().getRight()
                        + ", '" + friendRequest.getDate() + "')");
            }
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return friendRequest;

    }

    @Override
    public FriendRequest delete(Tuple<Long, Long> id) {
        return null;
    }

    @Override
    public FriendRequest update(FriendRequest friendRequest) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE friend_requests SET status = '"
                    + friendRequest.getStatus()
                    + "' WHERE from_user = " + friendRequest.getId().getLeft()
                    + " AND to_user = " + friendRequest.getId().getRight());
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return friendRequest;

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Page<FriendRequest> findAll(Pageable pageable) {
        return null;
    }
}
