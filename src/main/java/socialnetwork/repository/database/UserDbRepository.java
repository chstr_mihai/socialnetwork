package socialnetwork.repository.database;

import socialnetwork.domain.User;
import socialnetwork.exceptions.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDbRepository implements PagingRepository<Long, User> {
    private final String url;
    private final String username;
    private final String password;
    Connection connection;
    Long currentId;

    public UserDbRepository(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();
        initIdValue();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private void initIdValue() {

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT MAX(uid) AS max_id FROM users");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            currentId = resultSet.getLong("max_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void addFriendsToOneUser(User user) {

        try {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships WHERE u1id = "
                    + user.getId() + " OR u2id = " + user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                Long friendId;
                if (resultSet.getLong("u1id") != user.getId())
                    friendId = resultSet.getLong("u1id");
                else
                    friendId = resultSet.getLong("u2id");
                statement = connection.prepareStatement("SELECT * FROM users WHERE uid = " + friendId);
                ResultSet resultSet1 = statement.executeQuery();
                resultSet1.next();

                user.addFriend(new User(resultSet1.getString("first_name")
                        , resultSet1.getString("last_name"), resultSet1.getLong("uid")));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public User findOne(Long uid) {

        User user = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * from users where uid = " + uid);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next())
                return null;
            Long id = resultSet.getLong("uid");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String userPassword = resultSet.getString("user_password");
            String nickname = resultSet.getString("nickname");

            user = new User(firstName, lastName, id, userPassword, nickname);
            addFriendsToOneUser(user);

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return user;

    }

    private void addFriendsToAll(List<User> users) {

        Map<Long, User> userMap = new HashMap<>();
        users.forEach(u -> userMap.put(u.getId(), u));

        userMap.forEach((id, user) -> {
            try {
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships WHERE u1id = "
                        + id + " OR u2id = " + id);
                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {

                    if(resultSet.getLong("u1id") != id)
                        user.addFriend(userMap.get(resultSet.getLong("u1id")));
                    else
                        user.addFriend(userMap.get(resultSet.getLong("u2id")));

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        });

    }

    @Override
    public Iterable<User> findAll() {

        List<User> users = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * from users");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("uid");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");

                User user = new User(firstName, lastName, id);
                users.add(user);

            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        addFriendsToAll(users);
        return users;

    }

    @Override
    public User save(User user) {

        try {

            PreparedStatement statement = connection.prepareStatement("SELECT user_password FROM users WHERE user_password = '"
                    + user.getPassword() + "'");
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                throw new RepoException("The password you chose is already in use!");

            currentId += 1;
            user.setId(currentId);

            statement = connection.prepareStatement("INSERT INTO users values(" + currentId
                    + ", '" + user.getFirstName() + "', '" + user.getLastName() + "', '" + user.getPassword() +"', '"
                    + user.getChildhoodNickname() + "')");
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        System.out.println(user);
        return user;

    }

    private User verifyIfExists(Long uid) {

        User user = null;
        try{
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE uid = " + uid);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                user = new User(resultSet.getString("first_name"), resultSet.getString("last_name")
                        , resultSet.getLong("uid"));

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }
        return user;

    }

    @Override
    public User delete(Long id) {

        User deleted_user = null;
        try {
            deleted_user = verifyIfExists(id);
            if (deleted_user == null)
                throw new RepoException("The user with id = " + id + " doesn't exist");

            PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE uid = " + id);
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return deleted_user;

    }

    @Override
    public User update(User user) {

        try {

            PreparedStatement statement = connection.prepareStatement("UPDATE users SET user_password = '" + user.getPassword()
                    + "' WHERE uid = " + user.getId());
            statement.execute();

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return null;
    }

    @Override
    public int size() {

        int len = 0;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS size FROM users");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            len = resultSet.getInt("size");
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return len;

    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return null;
    }

}
