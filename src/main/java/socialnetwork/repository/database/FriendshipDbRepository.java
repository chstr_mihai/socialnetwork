package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.exceptions.RepoException;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FriendshipDbRepository implements Repository<Tuple<Long, Long>, Friendship> {

    private final String url;
    private final String username;
    private final String password;
    Connection connection;

    public FriendshipDbRepository(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Friendship findOne(Tuple<Long, Long> fid) {
        Friendship friendship = null;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * from friendships where u1id = " + fid.getLeft() + "and u2id = " + fid.getRight());
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                statement = connection.prepareStatement(
                        "SELECT * from friendships where u2id = " + fid.getLeft() + "and u1id = " + fid.getRight());
                resultSet = statement.executeQuery();
                if (!resultSet.next())
                    return null;
            }

            Long u1id = resultSet.getLong("u1id");
            Long u2id = resultSet.getLong("u2id");
            LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

            friendship = new Friendship(u1id, u2id, date);

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return friendship;
    }

    @Override
    public Iterable<Friendship> findAll() {
        List<Friendship> friendships = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long u1id = resultSet.getLong("u1id");
                Long u2id = resultSet.getLong("u2id");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                Friendship friendship = new Friendship(u1id, u2id, date);
                friendships.add(friendship);
            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship friendship) {

        if (findOne(friendship.getId()) != null)
            throw new RepoException("The friendship already exists");

        try {

            PreparedStatement statement = connection.prepareStatement("INSERT INTO friendships values("
                    + friendship.getId().getLeft() + ", " + friendship.getId().getRight()
                    + ", '" + friendship.getDate() + "')");
            statement.execute();

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return friendship;

    }

    @Override
    public Friendship delete(Tuple<Long, Long> id) {
        Friendship deleted_friendship = null;
        try {

            deleted_friendship = findOne(id);
            if (deleted_friendship == null)
                throw new RepoException("The friendship with id = " + id + " doesn't exist");

            PreparedStatement statement = connection.prepareStatement("DELETE FROM friendships WHERE u1id = "
                    + deleted_friendship.getId().getLeft() + "and u2id = " + deleted_friendship.getId().getRight());
            statement.execute();

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return deleted_friendship;
    }

    @Override
    public Friendship update(Friendship entity) {
        return null;
    }

    @Override
    public int size() {

        int len = 0;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS size FROM friendships");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            len = resultSet.getInt("size");
        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return len;

    }

}
