package socialnetwork.repository.database;

import socialnetwork.domain.Participant;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ParticipantsDbRepository implements Repository<Tuple<Long, Long>, Participant> {

    private final String url;
    private final String username;
    private final String password;
    Connection connection;

    public ParticipantsDbRepository(String url, String username, String password) {

        this.url = url;
        this.username = username;
        this.password = password;

        initConnection();

    }

    private void initConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Participant findOne(Tuple<Long, Long> pid) {

        Participant participant = null;
        try{

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM participants WHERE eid = "
                    + pid.getLeft() + " and uid = " + pid.getRight());
            ResultSet resultSet = statement.executeQuery();

            if(!resultSet.next())
                return null;

            participant = new Participant(resultSet.getLong("eid"), resultSet.getLong("uid")
                    , resultSet.getBoolean("notifications"));

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return participant;

    }

    @Override
    public Iterable<Participant> findAll() {

        List<Participant> participants = new ArrayList<>();
        try{

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM participants");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Long eid = resultSet.getLong("eid");
                Long uid = resultSet.getLong("uid");
                boolean notifications = resultSet.getBoolean("notifications");
                participants.add(new Participant(eid, uid, notifications));

            }

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return participants;
    }

    @Override
    public Participant save(Participant participant) {

        try {

            PreparedStatement statement = connection.prepareStatement("INSERT INTO participants VALUES("
                    + participant.getId().getLeft() + ", " + participant.getId().getRight() + ", true)");
            statement.execute();

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return participant;

    }

    @Override
    public Participant delete(Tuple<Long, Long> pid) {

        Participant participant = findOne(pid);
        if(participant != null) {
            try {

                PreparedStatement statement = connection.prepareStatement("DELETE FROM participants WHERE eid = " + pid.getLeft()
                        + " and uid = " + pid.getRight());
                statement.execute();

            } catch (SQLException e) {
                System.out.println("Failed to execute query");
            }
        }

        return participant;

    }

    @Override
    public Participant update(Participant participant) {

        participant = findOne(participant.getId());
        participant.setNotificationsOn(!participant.isNotificationsOn());
        try {

            PreparedStatement statement = connection.prepareStatement("UPDATE participants SET notifications = "
                    + participant.isNotificationsOn() + " WHERE eid = " + participant.getId().getLeft() + " and uid = "
                    + participant.getId().getRight());
            statement.execute();

        } catch (SQLException e) {
            System.out.println("Failed to execute query");
        }

        return participant;

    }

    @Override
    public int size() {
        return 0;
    }
}
